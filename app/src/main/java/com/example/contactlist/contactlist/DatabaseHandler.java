package com.example.contactlist.contactlist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shelley on 10/21/2015.
 * This will help handle the SQLite stuff.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String TAG = "__DatabaseHandler: "; // for logcat

    private static final int DATABASE_VERSION = 1;

    // the names of the tables and keys
    private static final String DATABASE_NAME = "contactList",
            TABLE_CONTACTS = "contacts",
            KEY_ID = "id",
            KEY_NAME = "name",
            KEY_ADDRESS = "address",
            KEY_BIRTHDAY = "birthday",
            KEY_IMAGE = "image",
            KEY_BLACKLIST = "blacklist", // 0 (false), 1 (true)
            TABLE_PHONE_NUMBERS = "phoneNumbers",
            KEY_PHONE_TABLE_ID = "phoneTableID",
            KEY_PHONE_ID = "phoneID",
            KEY_PHONE_NUM = "phoneNum",
            KEY_PHONE_LABEL = "phoneLabel",
            TABLE_EMAIL_ADDRESS = "emailAddresses",
            KEY_EMAIL_TABLE_ID = "emailTableID",
            KEY_EMAIL_ID = "emailID",
            KEY_EMAIL = "email",
            KEY_EMAIL_LABEL = "emailLabel",
            TABLE_GROUPS = "groups",
            KEY_GROUP_NAME = "groupName",
            KEY_GROUP_ID = "groupID",
            TABLE_CONTACT_GROUPS = "tableContactGroups",
            KEY_CONTACT_GROUPS_TABLE_ID = "contactGroupsTableID",
            KEY_CG_CONTACT_ID = "tableContactGroupsContactID",
            KEY_CG_GROUP_ID = "tableContactGroupsGroupID";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Create the database tables to use with the contact application.
     * @param db The database that we will create the tables in.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "Entered onCreate()");
        db.execSQL("CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_NAME + " TEXT,"
                + KEY_ADDRESS + " TEXT,"
                + KEY_BIRTHDAY + " TEXT,"
                + KEY_IMAGE + " TEXT,"
                + KEY_BLACKLIST + " INTEGER);");

        db.execSQL("CREATE TABLE " + TABLE_PHONE_NUMBERS + "("
                + KEY_PHONE_TABLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_PHONE_ID + " INT,"
                + KEY_PHONE_NUM + " TEXT,"
                + KEY_PHONE_LABEL + " TEXT);");

        db.execSQL("CREATE TABLE " + TABLE_EMAIL_ADDRESS + "("
                + KEY_EMAIL_TABLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_EMAIL_ID + " INT,"
                + KEY_EMAIL + " TEXT,"
                + KEY_EMAIL_LABEL + " TEXT);");

        db.execSQL("CREATE TABLE " + TABLE_GROUPS + "("
                + KEY_GROUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_GROUP_NAME + " TEXT);");

        db.execSQL("CREATE TABLE " + TABLE_CONTACT_GROUPS + "("
                + KEY_CONTACT_GROUPS_TABLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_CG_CONTACT_ID + " INT,"
                + KEY_CG_GROUP_ID + " INT);");
    }

    /**
     * Upgrade the database if needed.
     * @param db the database we want to update.
     * @param oldVersion the old version of the database.
     * @param newVersion the new version of the databse.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "Entered onUpgrade()");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PHONE_NUMBERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EMAIL_ADDRESS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUPS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACT_GROUPS);

        onCreate(db);
    }

    /**
     * Create a contact and store it in the database.
     * @param contact The contact to be stored.
     */
    public void createContact(Contact contact) {
        Log.d(TAG, "Entered createContact()");

        Log.d(TAG, "ContactID: " + contact.getId());
        SQLiteDatabase db = getWritableDatabase();

        ContentValues contactValues = new ContentValues();

        contactValues.put(KEY_NAME, contact.getName());
        contactValues.put(KEY_ADDRESS, contact.getAddress());
        contactValues.put(KEY_BIRTHDAY, contact.getBirthday());
        contactValues.put("image", contact.getImage());
        contactValues.put(KEY_BLACKLIST, contact.isBlackListed()); // false by default. Change in edit.

        long contactID = db.insert(TABLE_CONTACTS, null, contactValues);
        db.close();

        // add the phone numbers to the table
        List<Phone> phoneNumbers = contact.getPhoneNumbers();
        for (int i = 0; i < phoneNumbers.size(); i++) {
            phoneNumbers.get(i).setPhoneID((int) contactID); // set the correct id
            addContactPhoneNumber(phoneNumbers.get(i));
        }

        // add the email addresses to the table
        List<Email> emailAddresses = contact.getEmailAddresses();
        for (int i = 0; i < emailAddresses.size(); i++) {
            emailAddresses.get(i).setEmailID((int) contactID);
            addContactEmailAddress(emailAddresses.get(i));
        }
    }

    /**
     * Add the contact's phone number to the phone number table.
     * @param phone The phone number to be added.
     */
    public void addContactPhoneNumber(Phone phone) {
        Log.d(TAG, "Entered addContactPhoneNumber()");
        int phoneTableID = getPhoneCount();
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        Log.d(TAG, "tableID: " + phoneTableID + "; phoneID: " + phone.getPhoneID() + "; num: " + phone.getPhoneNumber() + "; label: " + phone.getPhoneLabel());

        values.put(KEY_PHONE_ID, phone.getPhoneID());
        values.put(KEY_PHONE_LABEL, phone.getPhoneLabel());
        values.put(KEY_PHONE_NUM, phone.getPhoneNumber());

        db.insert(TABLE_PHONE_NUMBERS, null, values);
        db.close();
    }

    /**
     * Add the contact's email address to the email table.
     * @param email The email to add to the table.
     */
    public void addContactEmailAddress(Email email) {
        Log.d(TAG, "Entered addContactEmailAddress()");
        int emailTableID = getEmailCount();
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        Log.d(TAG, "tableID: " + emailTableID + "; emailID: " + email.getEmailID() + "; email: " + email.getEmailAddress() + "; label: " + email.getEmailLabel());

        values.put(KEY_EMAIL_ID, email.getEmailID());
        values.put(KEY_EMAIL_LABEL, email.getEmailLabel());
        values.put(KEY_EMAIL, email.getEmailAddress());

        db.insert(TABLE_EMAIL_ADDRESS, null, values);
        db.close();
    }

    /**
     * Create a new Group and add it to the database with all the contacts added.
     * @param group The group to add to the database.
     */
    public void createGroup(Group group) {
        Log.d(TAG, "Entered createGroup()");
        List<Contact> groupContactList = group.getGroupContactList();
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_GROUP_ID, group.getGroupID());
        values.put(KEY_GROUP_NAME, group.getGroupName());

        long groupID = db.insert(TABLE_GROUPS, null, values);
        db.close();

        // add contact groups
        for (int i = 0; i < groupContactList.size(); i++) {
            addToGroup((int) groupID, groupContactList.get(i).getId());
        }
    }

    /**
     * Add a contact to a group.
     * @param groupID The ID of the group to add the contact to.
     * @param contactID The ID of the contact we want to add to the group.
     */
    public void addToGroup(int groupID, int contactID) {
        Log.d(TAG, "Entered addToGroup()");
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        Log.d(TAG, "contactID: " + contactID + "; groupID: " + groupID + ";");

        values.put(KEY_CG_CONTACT_ID, contactID);
        values.put(KEY_CG_GROUP_ID, groupID);

        db.insert(TABLE_CONTACT_GROUPS, null, values);
        db.close();
    }

    /**
     * Get the contact's information from the database.
     * @param id The ID of the contact. We use this to look up the contact in the database.
     * @return The found contact.
     */
    public Contact getContact(int id) {
        Log.d(TAG, "getContact(" + id + ")");
        SQLiteDatabase db = getReadableDatabase();
        Contact contact = null;
        List<Phone> phoneNumbers = null;
        List<Email> emailAddresses = null;

        Cursor cursor = db.query(TABLE_CONTACTS, new String[] { KEY_ID, KEY_NAME, KEY_ADDRESS,
                        KEY_BIRTHDAY, KEY_IMAGE, KEY_BLACKLIST }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null );

        if (cursor != null && cursor.moveToFirst()) {
            phoneNumbers = getPhoneNumbers(Integer.parseInt(cursor.getString(0)));
            emailAddresses = getEmailAddresses(Integer.parseInt(cursor.getString(0)));
            contact = new Contact(Integer.parseInt(cursor.getString(0)), cursor.getString(1),
                    phoneNumbers, emailAddresses, cursor.getString(2), cursor.getString(3),
                    cursor.getString(4), Integer.parseInt(cursor.getString(5)));
        }

        db.close();
        if (cursor != null) {
            cursor.close();
        }
        return contact;
    }

    /**
     * Get the group information from the database.
     * @param id The ID of the Group. We use this to look up the Group in the database.
     * @return The found Group.
     */
    public Group getGroup(int id) {
        Log.d(TAG, "getGroup(" + id + ")");
        SQLiteDatabase db = getReadableDatabase();
        Group group = null;
        List<Contact> groupContactList;

        Cursor cursor = db.query(TABLE_GROUPS, new String[]{KEY_GROUP_ID, KEY_GROUP_NAME},
                KEY_GROUP_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            groupContactList = getGroupContactList(Integer.parseInt(cursor.getString(0)));
            group = new Group(Integer.parseInt(cursor.getString(0)), cursor.getString(1), groupContactList);
        }

        db.close();
        if (cursor != null) {
            cursor.close();
        }
        return group;
    }

    /**
     * Delete a contact and their additional information from all related tables in the database.
     * @param contact The contact to be deleted.
     */
    public void deleteContact(Contact contact) {
        Log.d(TAG, "Entered deleteContact()");
        SQLiteDatabase db = getWritableDatabase();

        db.delete(TABLE_CONTACTS, KEY_ID + "=?", new String[]{String.valueOf(contact.getId())});
        db.delete(TABLE_PHONE_NUMBERS, KEY_PHONE_ID + "=?", new String[]{String.valueOf(contact.getId())});
        db.delete(TABLE_EMAIL_ADDRESS, KEY_EMAIL_ID + "=?", new String[]{String.valueOf(contact.getId())});
        db.delete(TABLE_CONTACT_GROUPS, KEY_CG_CONTACT_ID + "=?", new String[]{String.valueOf(contact.getId())});
        db.close();
    }

    /**
     * Delete a group and the associated information.
     * @param group The Group to be deleted.
     */
    public void deleteGroup(Group group) {
        Log.d(TAG, "Entered deleteGroup()");
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_GROUPS, KEY_GROUP_ID + "=?", new String[]{String.valueOf(group.getGroupID())});
        db.delete(TABLE_CONTACT_GROUPS, KEY_CG_GROUP_ID + "=?", new String[]{String.valueOf(group.getGroupID())});
        db.close();
    }

    /**
     * Get the total number of contacts in the user's address book.
     * @return The total number of contacts.
     */
    public int getContactsCount() {
        Log.d(TAG, "Entered getContactsCount()");
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_CONTACTS, null);
        int count = cursor.getCount();
        db.close();
        cursor.close();
        return count;
    }

    /**
     * Get the number of phone numbers in the phone number table (used for database calculations).
     * @return The total number of emails.
     */
    public int getPhoneCount() {
        Log.d(TAG, "Entered getPhoneCount()");
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PHONE_NUMBERS, null);
        int count = cursor.getCount();
        db.close();
        cursor.close();
        return count;
    }

    /**
     * Get the number of emails in the email table (used for database calculations).
     * @return The total number of emails.
     */
    public int getEmailCount() {
        Log.d(TAG, "Entered getEmailCount()");
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_EMAIL_ADDRESS, null);
        int count = cursor.getCount();
        db.close();
        cursor.close();
        return count;
    }

    /**
     * Get the number of group entries in the group table (used for database calculations).
     * @return The total number of group entries.
     */
    public int getGroupCount() {
        Log.d(TAG, "Entered getGroupCount()");
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_GROUPS, null);
        int count = cursor.getCount();
        db.close();
        cursor.close();
        return count;
    }

    /**
     * Get all the contacts in the form of an ArrayList.
     * @return The ArrayList of all contacts.
     */
    public List<Contact> getAllContacts() {
        Log.d(TAG, "Entered getAllContacts()");
        List<Contact> contacts = new ArrayList<>();
        List<Phone> phoneNumbers = null;
        List<Email> emailAddresses = null;

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_CONTACTS, null);

        if (cursor.moveToFirst()) {
            do {
                phoneNumbers = getPhoneNumbers(Integer.parseInt(cursor.getString(0)));
                emailAddresses = getEmailAddresses(Integer.parseInt(cursor.getString(0)));
                contacts.add(new Contact(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), phoneNumbers, emailAddresses, cursor.getString(2),
                        cursor.getString(3), cursor.getString(4),
                        Integer.parseInt(cursor.getString(5))));
                phoneNumbers = null; // in case previous contact had no phone numbers
                emailAddresses = null; // in case previous contact had no email addresses
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return contacts;
    }

    /**
     * Get all the groups in the form of an ArrayList.
     * @return The ArrayList of all groups.
     */
    public List<Group> getAllGroups() {
        Log.d(TAG, "Entered getAllGroups()");
        List<Group> groups = new ArrayList<>();

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_GROUPS, null);

        if (cursor.moveToFirst()) {
            Log.d(TAG, "getAllGroups() ID: " + Integer.parseInt(cursor.getString(0))
                    + "; Name: " + cursor.getString(1));
            do {
                groups.add(getGroup(Integer.parseInt(cursor.getString(0))));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return groups;
    }

    /**
     * Get an ArrayList of all phone numbers for a contact.
     * @param id The contact's id.
     * @return An ArrayList of all phone numbers for that contact.
     */
    public List<Phone> getPhoneNumbers(int id) {
        List<Phone> phoneNumbers = new ArrayList<>();

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PHONE_NUMBERS + " WHERE "
                + KEY_PHONE_ID + " = " + id, null);

        if (cursor.moveToFirst()) {
            //Log.d(TAG, "getPhoneNumbers() Label: " + cursor.getString(2) + "; Num: " + cursor.getString(3));
            do {
                phoneNumbers.add(new Phone(Integer.parseInt(cursor.getString(1)),
                        cursor.getString(3), cursor.getString(2)));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return phoneNumbers;
    }

    /**
     * Get an ArrayList of all email addresses for a contact.
     * @param id The ID of the contact.
     * @return An ArrayList of all email addresses for a contact.
     */
    public List<Email> getEmailAddresses(int id) {
        //Log.d(TAG, "Entered getEmailAddresses(id=" + id + ")");
        List<Email> emailAddresses = new ArrayList<>();

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_EMAIL_ADDRESS + " WHERE "
                + KEY_EMAIL_ID + " = " + id, null);

        if (cursor.moveToFirst()) {
            do {
                emailAddresses.add(new Email(Integer.parseInt(cursor.getString(1)),
                        cursor.getString(3), cursor.getString(2)));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return emailAddresses;
    }

    /**
     * Get an ArrayList of all the contacts in a group.
     * @param groupID The ID of the group.
     * @return An ArrayList of all contacts within that group.
     */
    public List<Contact> getGroupContactList(int groupID) {
        Log.d(TAG, "Entered getGroupContactList(id=" + groupID + ")");
        List<Contact> groupContactList = new ArrayList<>();

        SQLiteDatabase db = getWritableDatabase();

        Cursor cursor = db.query(TABLE_CONTACT_GROUPS, new String[]{KEY_CG_GROUP_ID, KEY_CG_CONTACT_ID},
                KEY_CG_GROUP_ID + "=?", new String[]{String.valueOf(groupID)}, null, null, null, null);

        if (cursor.moveToFirst()) {
            Log.d(TAG, "getGroupContactList() gID: " + cursor.getString(0) + "; cID: " + cursor.getString(1));
            Contact contact;
            do {
                contact = getContact(Integer.parseInt(cursor.getString(1)));
                groupContactList.add(contact);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return groupContactList;
    }

    /**
     * Get an ArrayList of all the Groups associated with a Contact.
     * @param contactID The ID of the Contact.
     * @return An ArrayList of all Groups associated with that Contact.
     */
    public List<Group> getContactGroupList(int contactID) {
        Log.d(TAG, "Entered getContactGroupList(contactID=" + contactID + ")");
        List<Group> contactGroupList = new ArrayList<>();

        SQLiteDatabase db = getWritableDatabase();

        Cursor cursor = db.query(TABLE_CONTACT_GROUPS, new String[]{KEY_CG_GROUP_ID, KEY_CG_CONTACT_ID},
                KEY_CG_CONTACT_ID + "=?", new String[]{String.valueOf(contactID)}, null, null, null, null);

        if (cursor.moveToFirst()) {
            Log.d(TAG, "getContactGroupList() gID: " + cursor.getString(0) + "; cID: " + cursor.getString(1));
            do {
                contactGroupList.add(getGroup(Integer.parseInt(cursor.getString(0))));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return contactGroupList;
    }

    /**
     * Check to see if that contact already exists. If they do, do not create new contact!
     * @param contact The contact we want to check for existance.
     * @return Return true if contact exists, false if contact does not exist.
     */
    public boolean contactExists(Contact contact) {
        Log.d(TAG, "Checking to see if " + contact.getName() + " exists.");
        List<Contact> contactList = getAllContacts();
        String name = contact.getName();
        int contactCount = contactList.size();

        for (int i = 0; i < contactCount; i++) {
            if (name.compareToIgnoreCase(contactList.get(i).getName()) == 0) {
                Log.d(TAG, "Contact " + contact.getName() + " exists.");
                return true;
            }
        }
        Log.d(TAG, "Contact " + contact.getName() + " does not exist.");
        return false;
    }

    /**
     * Check to see if that group already exists. If they do, do not create new group!
     * @param group The group we want to check for existance.
     * @return Return true if group exists, false if group does not exist.
     */
    public boolean groupExists(Group group) {
        Log.d(TAG, "Checking to see if " + group.getGroupName() + " exists.");
        List<Group> groups = getAllGroups();
        String name = group.getGroupName();

        for (int i = 0; i < groups.size(); i++) {
            if (name.compareToIgnoreCase(groups.get(i).getGroupName()) == 0) {
                Log.d(TAG, "Group " + group.getGroupName() + " exists.");
                return true;
            }
        }
        Log.d(TAG, "Group " + group.getGroupName() + " does not exist.");
        return false;
    }

    /**
     * Update an existing contact in the databse when using EditContactActivity.java.
     * @param contact The contact information that we want to update.
     * @return the rows affected.
     */
    public int updateContact(Contact contact) {
        Log.d(TAG, "Entered updateContact()");
        SQLiteDatabase db = getWritableDatabase();

        ContentValues contactValues = new ContentValues();

        // store the contact values
        contactValues.put(KEY_NAME, contact.getName());
        contactValues.put(KEY_ADDRESS, contact.getAddress());
        contactValues.put(KEY_BIRTHDAY, contact.getBirthday());
        contactValues.put("image", contact.getImage());
        contactValues.put(KEY_BLACKLIST, contact.isBlackListed());

        // it's probably easier to just delete phone numbers and emails and re-add them.
        // remove old phone numbers from database
        db.delete(TABLE_PHONE_NUMBERS, KEY_PHONE_ID + "=?", new String[]{String.valueOf(contact.getId())});

        // remove old email addresses from database
        db.delete(TABLE_EMAIL_ADDRESS, KEY_EMAIL_ID + "=?", new String[]{String.valueOf(contact.getId())});

        // re-add the phone numbers and emails after the db.close() to avoid errors.

        int rowsAffected = db.update(TABLE_CONTACTS, contactValues, KEY_ID + "=?",
                new String[]{String.valueOf(contact.getId())});
        db.close();

        // add the phone numbers to the table
        List<Phone> phoneNumbers = contact.getPhoneNumbers();
        for (int i = 0; i < phoneNumbers.size(); i++) {
            phoneNumbers.get(i).setPhoneID(contact.getId()); // set the correct id
            addContactPhoneNumber(phoneNumbers.get(i));
        }

        // add the email addresses to the table
        List<Email> emailAddresses = contact.getEmailAddresses();
        for (int i = 0; i < emailAddresses.size(); i++) {
            emailAddresses.get(i).setEmailID(contact.getId());
            addContactEmailAddress(emailAddresses.get(i));
        }

        return rowsAffected;
    }

    /**
     * Update an existing Group in the databse when using EditGroupActivity.java.
     * @param group The Group information that we want to update.
     * @return the rows affected.
     */
    public int updateGroup(Group group) {
        Log.d(TAG, "Entered updateGroup()");
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_GROUP_ID, group.getGroupID());
        values.put(KEY_GROUP_NAME, group.getGroupName());

        // delete the group contact list in databse and re-add the new one
        db.delete(TABLE_CONTACT_GROUPS, KEY_CG_GROUP_ID + "=?", new String[]{String.valueOf(group.getGroupID())});

        // re-add the group contacts after the db.close() to avoid errors.

        int rowsAffected = db.update(TABLE_GROUPS, values, KEY_GROUP_ID + "=?",
                new String[]{String.valueOf(group.getGroupID())});
        db.close();

        // add contact groups
        List<Contact> groupContactList = group.getGroupContactList();
        for (int i = 0; i < groupContactList.size(); i++) {
            addToGroup(group.getGroupID(), groupContactList.get(i).getId());
        }

        return rowsAffected;
    }
}