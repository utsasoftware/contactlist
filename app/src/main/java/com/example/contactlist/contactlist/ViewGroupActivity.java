package com.example.contactlist.contactlist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

/**
 * View an individual group list of contacts.
 */
public class ViewGroupActivity extends AppCompatActivity {

    private static final String TAG = "__ViewGroupActivity: ";

    final Context context = this; // used for delete group dialog box

    DatabaseHandler dbHandler; // handler for SQLite database
    ListView groupListView; // the list of all groups
    ContactListAdapter adapter; // used for rendering the list of contacts
    List<Contact> contactList; // the list of contacts in the group
    Group group; // the current group we are viewing

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_group);

        int groupID = getIntent().getExtras().getInt("currentGroupID");
        Log.d(TAG, "The group id extra was: " + groupID);

        dbHandler = new DatabaseHandler(getApplicationContext());
        groupListView = (ListView) findViewById(R.id.groupListView);
        TextView groupName = (TextView) findViewById(R.id.groupName);

        group = dbHandler.getGroup(groupID);
        contactList = group.getGroupContactList();
        if (contactList != null) {
            Collections.sort(contactList, new ContactNameComparator()); // sort contacts alphabetically
        } else {
            Log.d(TAG, "The list was null!!!");
        }

        // set the group name
        groupName.setText(group.getGroupName());

        // render the group list
        adapter = new ContactListAdapter(this, contactList);
        groupListView.setAdapter(adapter);

        /**
         * Listener for the Edit Group button in activity_view_group.xml
         */
        final Button buttonEditGroup = (Button) findViewById(R.id.buttonEditGroup);
        buttonEditGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Clicked Edit Group Button");
                Intent editGroupActivity = new Intent(getApplicationContext(),
                        EditGroupActivity.class);
                editGroupActivity.putExtra("currentGroupID", group.getGroupID());
                startActivity(editGroupActivity);
                finish();
            }
        });

        /**
         * Listener for the Delete Group button in activity_view_group.xml
         */
        final Button buttonDeleteGroup = (Button) findViewById(R.id.buttonDeleteGroup);
        buttonDeleteGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Clicked Delete Group Button.");
                if (group == null) {
                    Log.d(TAG, "Group was null!!\n");
                }

                // set up an alert to ask if the user really wants to delete the contact
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                // set title
                alertDialogBuilder.setTitle("Delete Group");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Do you really want to delete this group? This will not delete your contacts.")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Log.d(TAG, "Entered onClick for Delete Group");
                                dbHandler.deleteGroup(group);
                                Toast.makeText(ViewGroupActivity.this, "Group "
                                                + group.getGroupName() + " deleted.",
                                        Toast.LENGTH_SHORT).show();
                                finish();
                                Log.d(TAG, "Killed ViewGroupActivity on group delete.");
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if "No" clicked, just close the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        }); // end button delete group
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();

        // update contact list
        contactList.clear();
        contactList.addAll(dbHandler.getGroupContactList(group.getGroupID()));
        adapter.notifyDataSetChanged();
    }

    /**
     * Want to return to the main contact list on back pressed.
     */
    public void onBackPressed() {
        this.finish();
        Log.d(TAG, "Killed ViewGroupActivity on back pressed.");
    }
}
