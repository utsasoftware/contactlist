package com.example.contactlist.contactlist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Shelley Klinzing on 11/6/15.
 * This activity helps create a group.
 *
 * We will need to create a reference to the listview and link it with the create_group_row.xml file.
 * Then we can pass the data items to the constuctor of the custom adapter. After that we will set
 * the adapter on our listview.
 */
public class CreateGroupActivity extends AppCompatActivity {

    private static final String TAG = "__CreateGroupActivity: "; // for logging
    private static final int CHECKED = 1; // checkbox checked
    private static final int UNCHECKED = 0; // checkbox unchecked

    List<Contact> contactList;
    DatabaseHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "Entered onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);

        dbHandler = new DatabaseHandler(getApplicationContext());
        contactList = dbHandler.getAllContacts();
        Collections.sort(contactList, new ContactNameComparator()); // sort contacts alphabetically
        final ListView contactListView = (ListView) findViewById(R.id.groupContactCheckBoxList);
        final Model[] allContacts = new Model[contactList.size()];

        // populate the listview with all contacts
        for (int i = 0; i < contactList.size(); i++) {
            allContacts[i] = new Model(contactList.get(i), UNCHECKED);
        }
        CustomAdapter adapter = new CustomAdapter(this, allContacts);
        contactListView.setAdapter(adapter);

        final Button buttonCreateGroup = (Button) findViewById(R.id.buttonAddGroup);
        buttonCreateGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "Clicked Create Group Button");

                // create the list of all contacts in the group
                List<Contact> groupContactList = new ArrayList<Contact>();
                for (int i = 0; i < contactList.size(); i++) {
                    if (allContacts[i].getValue() == CHECKED) {
                        Log.v(TAG, "Adding contact \"" + allContacts[i].getContact().getName() + "\"");
                        groupContactList.add(allContacts[i].getContact());
                    }
                }

                TextView groupName = (TextView) findViewById(R.id.groupName);

                // create the group
                Group group = new Group(dbHandler.getGroupCount(),
                        String.valueOf(groupName.getText()), groupContactList);

                // add group to database
                if (!dbHandler.groupExists(group)) {
                    dbHandler.createGroup(group);
                    Intent returnToMainActivity = new Intent(getApplicationContext(),
                            MainActivity.class);
                    startActivity(returnToMainActivity);
                    finish();
                    Toast.makeText(getApplicationContext(), "Group " + String.valueOf(groupName.getText())
                            + " has been created!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(getApplicationContext(), "Group " + String.valueOf(groupName.getText())
                        + " already exists. Plase use a differnet name.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * We want to place a data item inside the listview. Each data item will have a name and a
     * value which tells whether the item is selected or not. So, create a data model class. We
     * will create an array of data items and pass it to the adapter. The adapter will convert
     * these data items into view items.
     */
    private class Model{
        Contact contact;
        int value; /* 0 -&gt; checkbox disable, 1 -&gt; checkbox enable */

        Model(Contact contact, int value){
            this.contact = contact;
            this.value = value;
        }
        public String getName(){
            return contact.getName();
        }
        public int getValue(){ return this.value; }
        public void setValue(boolean value) {
            if(value == true) {
                this.value = CHECKED;
            } else {
                this.value = UNCHECKED;
            }
        }
        public Contact getContact() {
            return contact;
        }
    }

    /**
     * Developing a Custom Adapter to convert data items into view items.
     */
    private class CustomAdapter extends ArrayAdapter<Model> {
        Model[] allContacts = null; // holds the array of contacts
        Context context; // holds a reference to an activity which is using the custom adapter
        public CustomAdapter(Context context, Model[] resource) { // constructor
            super(context,R.layout.create_group_row,resource);
            this.context = context;
            this.allContacts = resource;
        }
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.create_group_row, parent, false);
            TextView name = (TextView) convertView.findViewById(R.id.contactName);
            CheckBox cb = (CheckBox) convertView.findViewById(R.id.checkBox);

            cb.setOnClickListener(new View.OnClickListener() {
                public void onClick(View checkBoxView) {
                    CheckBox cb = (CheckBox) checkBoxView;
                    allContacts[position].setValue(cb.isChecked());
                }
            });
            name.setText(allContacts[position].getName());
            // If the value is 1 (true), the item is selected. Set the value of the checkbox.
            if(allContacts[position].getValue() == CHECKED) {
                cb.setChecked(true);
            } else {
                cb.setChecked(false);
            }
            return convertView; // return the view item
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}