package com.example.contactlist.contactlist;

import java.util.Comparator;

/**
 * Created by Shelley on 11/12/2015.
 * Used to alphabetize the names of Grops.
 */
public class GroupNameComparator implements Comparator<Group> {

    @Override
    public int compare(Group a, Group b) {
        String nameA = a.getGroupName();
        String nameB = b.getGroupName();
        return nameA.replaceAll("\\s+", "").compareToIgnoreCase(nameB.replaceAll("\\s+", ""));
    }
}