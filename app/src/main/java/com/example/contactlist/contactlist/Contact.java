package com.example.contactlist.contactlist;

import android.media.Image;
import android.net.LinkAddress;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.ContactsContract;

import java.util.Date;
import java.util.List;

/**
 * Created by Shelley on 10/15/2015.
 * This class stores all the information for one contact, including:
 *      Name, phone number(s), email(s), address(s), birthday, Facebook link,
 *      contact image, group(s).
 */
public class Contact {

    private String name;                // the contact's first and last name, if applicable.
    private List<Phone> phoneNumbers;   // an array list of the contact's phone numbers
    private List<Email> emailAddresses; // an array list of email addresses
    private String address;             // the physical address of the contact
    private String encodedImageString;  // this will be the encoded contact image. Decode to view.
    private int id;                     // this will be used for the database table
    private String birthday;            // the contact's birthday string, mm-dd-yyy
    private int blackList = 0;          // is the contact blacklisted? False (0) by default.
    //private String _ringTone;         // TODO: implement ringtone

    public Contact(int id, String name, List<Phone> phoneNumbers, List<Email> emailAddresses,
                   String address, String birthday, String encodedImageString, int blackList) {
        this.id = id;
        this.name = name;
        this.phoneNumbers = phoneNumbers;
        this.emailAddresses = emailAddresses;
        this.address = address;
        this.birthday = birthday;
        this.encodedImageString = encodedImageString;
        this.blackList = blackList;
    }

    /**
     * Get the ID of the contact.
     * @return The contact ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Get the name of the contact.
     * @return the contact name.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of the contact.
     * @param name The new contact name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get all associated email addresses for a contact.
     * @return The ArrayList of all email addresses.
     */
    public List<Email> getEmailAddresses() {
        return emailAddresses;
    }

    /**
     * Update the list of email addresses for a contact.
     * @param emailAddresses The new ArrayList of email addresses for a contact.
     */
    public void updateEmailAddresses(List<Email> emailAddresses) {
        this.emailAddresses = emailAddresses;
    }

    /**
     * Get the address for a contact.
     * @return the address of that contact.
     */
    public String getAddress() {
        return address;
    }

    /**
     * Update a contact's address.
     * @param address The new address for the contact.
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * The list of all phone numbers associated with a contact.
     * @return The ArrayList of the contact's phone numbers.
     */
    public List<Phone> getPhoneNumbers() {
        return phoneNumbers;
    }

    /**
     * Update the contact's phone numbers.
     * @param phoneNumbers The ArrayList of the updated phone numbers for a contact.
     */
    public void updatePhoneNumbers(List<Phone> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    /**
     * Get the contact's birthday.
     * @return The contact's birthday.
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * Update the contact's birthday.
     * @param birthday The updated birthday for the contact.
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    /**
     * Get the profile picture for a contact.
     * @return The encoded image string for the contact's profile picture.
     */
    public String getImage() {
        return encodedImageString;
    }

    /**
     * Update the contact's profile picture.
     * @param encodedImageString The encoded image String of the contact's profile picture that we
     *                           can decode to view the contact's image.
     */
    public void setImage(String encodedImageString) {
        this.encodedImageString = encodedImageString;
    }

    /**
     * This method returns 1 (true) if the contact is on the black list, and 0 (false) if the
     * contact is not on the black list.
     * @return 1 if the contact is on the blacklist, 0 if the contact is not on the blacklist.
     */
    public int isBlackListed() {
        return blackList;
    }

    /**
     * Ability to set the contact's status on the blacklist.
     * @param blackList 1 if the contact is on the blacklist, 0 if they are not.
     */
    public void setBlacklist(int blackList) {
        this.blackList = blackList;
    }
}
