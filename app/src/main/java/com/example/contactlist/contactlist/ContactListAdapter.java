package com.example.contactlist.contactlist;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View.OnClickListener;

/**
 * Created by Shelley on 11/11/2015.
 * This class helps us populate the address book list.
 * This class also helps us search and sort that list.
 */
public class ContactListAdapter extends BaseAdapter {

    private static final String TAG = "__ListAdapter: ";

    // Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private List<Contact> contactList = null;
    private ArrayList<Contact> arraylist;

    public ContactListAdapter(Context context,
                              List<Contact> contactList) {
        mContext = context;
        this.contactList = contactList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<Contact>();
        this.arraylist.addAll(contactList);
    }

    public class ViewHolder {
        TextView contactName;
        TextView phone;
        ImageView contactImage;
    }

    /**
     * Get the number of Contacts in the address book.
     * @return The total number of Contacts in the address book.
     */
    @Override
    public int getCount() {
        return contactList.size();
    }

    /**
     * Return the Contact at the position selected.
     * @param position the position in the adapter.
     * @return the Contact object at that position.
     */
    @Override
    public Contact getItem(int position) {
        return contactList.get(position);
    }

    /**
     * Get the ID of the item at position in the adapter.
     * @param position The position in the adapter we want to get the id of.
     * @return the ID of the object at that position.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        Log.d(TAG, "Entered ContactListAdapter class.");
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.view_contact_in_address_book, null);

            // locate the views
            holder.contactName = (TextView) view.findViewById(R.id.contactName);
            holder.phone = (TextView) view.findViewById(R.id.contactPhone);
            holder.contactImage = (ImageView) view.findViewById(R.id.ivContactImage);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        // Set the results into TextViews
        holder.contactName.setText(contactList.get(position).getName());

        List<Phone> phoneNumbers = contactList.get(position).getPhoneNumbers();
        if (phoneNumbers.size() > 0) {
            holder.phone.setVisibility(view.VISIBLE);
            holder.phone.setText(phoneNumbers.get(0).getPhoneNumber());
        }

        // Set the results into ImageView
        // decode and view contact image
        String encodedImg = contactList.get(position).getImage();
        byte[] bytarray = Base64.decode(encodedImg, Base64.DEFAULT);
        Bitmap contactImg = BitmapFactory.decodeByteArray(bytarray, 0, bytarray.length);
        holder.contactImage.setImageBitmap(contactImg);
        
        // Listen for ListView Item Click
        view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                // Send single item click data to SingleItemView Class
                Intent intent = new Intent(mContext, ViewContactActivity.class);
                // Pass all data rank
                intent.putExtra("currentContactID", (contactList.get(position).getId()));
                // Start SingleItemView Class
                mContext.startActivity(intent);
            }
        });

        return view;
    }

    /**
     * This function helps us alphabetize the list of contacts.
     * @param charText Used to compare contact names to see which should be listed first.
     */
    public void filter(String charText) {
        Log.d(TAG, "Entered Filter.");
        charText = charText.toLowerCase(Locale.getDefault());
        contactList.clear();
        if (charText.length() == 0) {
            contactList.addAll(arraylist);
        } else {
            for (Contact contact : arraylist) {
                if (contact.getName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    contactList.add(contact);
                }
            }
        }
        notifyDataSetChanged();
    }
}