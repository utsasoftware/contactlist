package com.example.contactlist.contactlist;

/**
 * Created by iramlee on 10/21/15.
 * This class represents a phone number for a contact. The phone
 * object is made up of a label and the phone number, both of
 * String type.
 */
public class Phone {
    private int phoneID;
    private String phoneLabel, phoneNumber;

    /*
    * Constructor:
    * ------------
    * Creates a new Phone object
    * */
    public Phone(int phoneID, String phoneLabel, String phoneNumber){
        this.phoneID = phoneID;
        this.phoneLabel = phoneLabel;
        this.phoneNumber = phoneNumber;
    }

    /*
    * Method: getPhoneID()
    * ------------------
    * Returns the Phone id, or the contact id associated with the phone number
    * */
    public int getPhoneID(){
        return phoneID;
    }

    /**
     * Set the phone id to be the same as the contact id so we can find it in the database.
     * @param phoneID The same id as the contact id.
     */
    public void setPhoneID (int phoneID) {
        this.phoneID = phoneID;
    }

    /*
    * Method: getPhoneNumber()
    * ------------------
    * Returns the Phone number as a string.
    * */
    public String getPhoneNumber(){
        return phoneNumber;
    }

    /*
    * Method: getPhoneLabel()
    * ------------------
    * Returns the Phone label as a string.
    * */
    public String getPhoneLabel(){
        return phoneLabel;
    }
}
