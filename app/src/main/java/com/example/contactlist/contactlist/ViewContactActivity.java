package com.example.contactlist.contactlist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shelley on 10/23/2015.
 * This class helps display a contact's detail when selected from the list view from MainActivity.
 */
public class ViewContactActivity extends AppCompatActivity{

    private static final String TAG = "__ViewContactActivity: "; // for logging
    private static final int BLACKLISTED_FALSE = 0;

    final Context context = this;
    Contact contact;
    List<Phone> phoneNumbers;
    List<Email> emailAddresses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Entered onCreate()\n");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_contact_detail);

        final DatabaseHandler dbHandler = new DatabaseHandler(getApplicationContext());
        int contactID = getIntent().getExtras().getInt("currentContactID");
        Log.d(TAG, "The contact id extra was: " + contactID);

        contact = dbHandler.getContact(contactID);
        if (contact != null) {
            String nameTest = contact.getName();
            Log.d(TAG,"Contact name is \"" + nameTest + "\"");
            Log.d(TAG, "ID (object): " + contact.getId() + "; ID (DB): " + contactID);
            
            // print the contact name
            TextView name = (TextView) findViewById(R.id.contactName);
            name.setText(contact.getName());

            // set up display stuff for phone numbers
            TextView phoneDisplayLabel = (TextView) findViewById(R.id.phoneNumbersDisplayLabel);
            TextView phone1 = (TextView) findViewById(R.id.phoneNumber1);
            TextView phone2 = (TextView) findViewById(R.id.phoneNumber2);
            TextView phone3 = (TextView) findViewById(R.id.phoneNumber3);
            TextView phone4 = (TextView) findViewById(R.id.phoneNumber4);
            List<TextView> phoneNumberTextViews= new ArrayList<TextView>();
            phoneNumberTextViews.add(phone1);
            phoneNumberTextViews.add(phone2);
            phoneNumberTextViews.add(phone3);
            phoneNumberTextViews.add(phone4);
            ImageButton buttonCallPhone1 = (ImageButton) findViewById(R.id.buttonCallPhone1);
            ImageButton buttonCallPhone2 = (ImageButton) findViewById(R.id.buttonCallPhone2);
            ImageButton buttonCallPhone3 = (ImageButton) findViewById(R.id.buttonCallPhone3);
            ImageButton buttonCallPhone4 = (ImageButton) findViewById(R.id.buttonCallPhone4);
            List<ImageView> callPhoneButtons = new ArrayList<>();
            callPhoneButtons.add(buttonCallPhone1);
            callPhoneButtons.add(buttonCallPhone2);
            callPhoneButtons.add(buttonCallPhone3);
            callPhoneButtons.add(buttonCallPhone4);
            ImageButton buttonMessagePhone1 = (ImageButton) findViewById(R.id.buttonMessagePhone1);
            ImageButton buttonMessagePhone2 = (ImageButton) findViewById(R.id.buttonMessagePhone2);
            ImageButton buttonMessagePhone3 = (ImageButton) findViewById(R.id.buttonMessagePhone3);
            ImageButton buttonMessagePhone4 = (ImageButton) findViewById(R.id.buttonMessagePhone4);
            List<ImageView> messagePhoneButtons = new ArrayList<>();
            messagePhoneButtons.add(buttonMessagePhone1);
            messagePhoneButtons.add(buttonMessagePhone2);
            messagePhoneButtons.add(buttonMessagePhone3);
            messagePhoneButtons.add(buttonMessagePhone4);

            // get the contact phone numbers
            phoneNumbers = contact.getPhoneNumbers();
            if (phoneNumbers.size() > 0) {
                for (int i = 0; i < phoneNumbers.size(); i++) {
                    if (!phoneNumbers.get(i).getPhoneNumber().equals("")) {
                        phoneDisplayLabel.setVisibility(View.VISIBLE);
                        phoneNumberTextViews.get(i).setVisibility(View.VISIBLE);
                        callPhoneButtons.get(i).setVisibility(View.VISIBLE);
                        messagePhoneButtons.get(i).setVisibility(View.VISIBLE);
                        phoneNumberTextViews.get(i).setText(phoneNumbers.get(i).getPhoneLabel() + ": "
                                + phoneNumbers.get(i).getPhoneNumber());
                    }
                }
            }

            // set up stuff to display contact email addresses
            TextView emailDisplayLabel = (TextView) findViewById(R.id.emailAddressesDisplayLabel);
            TextView email1 = (TextView) findViewById(R.id.email1);
            TextView email2 = (TextView) findViewById(R.id.email2);
            TextView email3 = (TextView) findViewById(R.id.email3);
            TextView email4 = (TextView) findViewById(R.id.email4);
            List<TextView> emailAddressTextViews= new ArrayList<TextView>();
            emailAddressTextViews.add(email1);
            emailAddressTextViews.add(email2);
            emailAddressTextViews.add(email3);
            emailAddressTextViews.add(email4);

            // get the contact email addresses
            emailAddresses = contact.getEmailAddresses();
            if (emailAddresses.size() > 0) {
                for (int i = 0; i < emailAddresses.size(); i++) {
                    if (!emailAddresses.get(i).getEmailAddress().equals("")) {
                        emailDisplayLabel.setVisibility(View.VISIBLE);
                        emailAddressTextViews.get(i).setVisibility(View.VISIBLE);
                        emailAddressTextViews.get(i).setText(emailAddresses.get(i).getEmailLabel() + ": "
                                + emailAddresses.get(i).getEmailAddress());
                    }
                }
            }

            if(!contact.getAddress().equals("")) { // if there is an actual address
                // get views
                TextView addressDisplayLabel = (TextView) findViewById(R.id.physicalAddressesDisplayLabel);
                TextView address = (TextView) findViewById(R.id.physicalAddress);
                // set to visible
                addressDisplayLabel.setVisibility(View.VISIBLE);
                address.setVisibility(View.VISIBLE);
                // display the address
                address.setText(contact.getAddress());
            }

            if(!contact.getBirthday().equals("")) { // if there is actually a birthday
                // get the views
                TextView birthdayDisplayLabel = (TextView) findViewById(R.id.birthdayDisplayLabel);
                TextView birthday = (TextView) findViewById(R.id.birthday);
                // set views to visible
                birthdayDisplayLabel.setVisibility(View.VISIBLE);
                birthday.setVisibility(View.VISIBLE);
                // display the birthday
                birthday.setText(contact.getBirthday());
            }

            if(contact.isBlackListed() == BLACKLISTED_FALSE) {
                TextView blackListDisplayLabel = (TextView) findViewById(R.id.blacListDisplayLabel);
                blackListDisplayLabel.setText("This contact is not on the blacklist.");
            } else {
                TextView blackListDisplayLabel = (TextView) findViewById(R.id.blacListDisplayLabel);
                blackListDisplayLabel.setText("This contact is on the blacklist.");
            }

            // decode and view contact image
            String encodedImg = contact.getImage();
            byte[] bytarray = Base64.decode(encodedImg, Base64.DEFAULT);
            Bitmap contactImg = BitmapFactory.decodeByteArray(bytarray, 0, bytarray.length);

            // display contact image
            ImageView ivContactImage = (ImageView) findViewById(R.id.ivContactImage);
            ivContactImage.setImageBitmap(contactImg);
        } else {
            Log.w("ViewContactActivity","Contact is null!");
        }

        /**
         * Listener for the Edit Contact button in activity_view_contact_detail_detail.xml
         */
        final Button buttonEditContact = (Button) findViewById(R.id.buttonEditContact);
        buttonEditContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Clicked Edit Contact Button");
                Intent editContactActivity = new Intent(getApplicationContext(),
                        EditContactActivity.class);
                editContactActivity.putExtra("currentContactID", contact.getId());
                startActivity(editContactActivity);
                finish();
            }
        });

        /**
         * Listener for the Delete Contact button in activity_view_contact_detail_detail.xml
         */
        final Button buttonDeleteContact = (Button) findViewById(R.id.buttonDeleteContact);
        buttonDeleteContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Clicked Delete Contact Button\n");
                if (contact == null) {
                    Log.d(TAG, "Contact was null!!\n");
                }

                // set up an alert to ask if the user really wants to delete the contact
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                // set title
                alertDialogBuilder.setTitle("Delete Contact");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Do you really want to delete this contact?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Log.d(TAG, "Entered onClick for Delete Contact\n");
                                dbHandler.deleteContact(contact);
                                finish();
                                Log.d(TAG, "Killed ViewContactActivity on contact delete.");
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if "No" clicked, just close the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        }); // end button delete contact
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Want to return to the main contact list on back pressed.
     */
    public void onBackPressed() {
        this.finish();
        Log.d(TAG, "Killed ViewContactActivity on back pressed.");
    }

    /**
     * This method will directly call a contact's phone number.
     * @param view activity_view_contact_detail view.
     */
    public void callPhone(View view) {
        Log.d(TAG, "Calling phone number...");
        String phone;
        Intent callPhone;
        switch(view.getId()) {
            case R.id.buttonCallPhone1:
                Log.d(TAG, "Clicked call phone 1.");
                callPhone = new Intent(Intent.ACTION_CALL);
                phone = "tel:" + phoneNumbers.get(0).getPhoneNumber();
                callPhone.setData(Uri.parse(phone));
                startActivity(callPhone);
                break;
            case R.id.buttonCallPhone2:
                Log.d(TAG, "Clicked call phone 2.");
                callPhone = new Intent(Intent.ACTION_CALL);
                phone = "tel:" + phoneNumbers.get(1).getPhoneNumber();
                callPhone.setData(Uri.parse(phone));
                startActivity(callPhone);
                break;
            case R.id.buttonCallPhone3:
                Log.d(TAG, "Clicked call phone 3.");
                callPhone = new Intent(Intent.ACTION_CALL);
                phone = "tel:" + phoneNumbers.get(2).getPhoneNumber();
                callPhone.setData(Uri.parse(phone));
                startActivity(callPhone);
                break;
            case R.id.buttonCallPhone4:
                Log.d(TAG, "Clicked call phone 4.");
                callPhone = new Intent(Intent.ACTION_CALL);
                phone = "tel:" + phoneNumbers.get(3).getPhoneNumber();
                callPhone.setData(Uri.parse(phone));
                startActivity(callPhone);
                break;
        } // end switch
    } // end callPhone()

    /**
     * This method will open the SMS manager so a contact can send a text.
     * @param view activity_view_contact_detail view.
     */
    public void sendMessage(View view) {
        Log.d(TAG, "Sending message...");
        Intent messagePhone;
        switch(view.getId()) {
            case R.id.buttonMessagePhone1:
                Log.d(TAG, "Clicked message phone 1.");
                messagePhone = new Intent(Intent.ACTION_VIEW);
                messagePhone.setType("vnd.android-dir/mms-sms");
                messagePhone.putExtra("address", phoneNumbers.get(0).getPhoneNumber());
                startActivity(messagePhone);
                break;
            case R.id.buttonMessagePhone2:
                Log.d(TAG, "Clicked message phone 2.");
                messagePhone = new Intent(Intent.ACTION_VIEW);
                messagePhone.setType("vnd.android-dir/mms-sms");
                messagePhone.putExtra("address", phoneNumbers.get(1).getPhoneNumber());
                startActivity(messagePhone);
                break;
            case R.id.buttonMessagePhone3:
                Log.d(TAG, "Clicked message phone 3.");
                messagePhone = new Intent(Intent.ACTION_VIEW);
                messagePhone.setType("vnd.android-dir/mms-sms");
                messagePhone.putExtra("address", phoneNumbers.get(2).getPhoneNumber());
                startActivity(messagePhone);
                break;
            case R.id.buttonMessagePhone4:
                Log.d(TAG, "Clicked message phone 4.");
                messagePhone = new Intent(Intent.ACTION_VIEW);
                messagePhone.setType("vnd.android-dir/mms-sms");
                messagePhone.putExtra("address", phoneNumbers.get(3).getPhoneNumber());
                startActivity(messagePhone);
                break;
        } // end switch
    } // end sendMessage()
}
