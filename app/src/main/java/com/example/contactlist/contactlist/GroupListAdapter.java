package com.example.contactlist.contactlist;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Shelley on 11/12/2015.
 * This helps us render the list of all groups.
 */
public class GroupListAdapter extends BaseAdapter {

    private static final String TAG = "__GroupListAdapter: ";

    // Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private List<Group> groupList = null;
    private ArrayList<Group> arraylist;

    public GroupListAdapter(Context context,
                       List<Group> groupList) {
        mContext = context;
        this.groupList = groupList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(groupList);
    }

    public class ViewHolder {
        TextView groupName;
        TextView groupContactCount;
    }

    /**
     * Get the number of Groups.
     * @return the total number of Groups.
     */
    @Override
    public int getCount() {
        return groupList.size();
    }

    /**
     * Get the Group object at the position selected.
     * @param position the position of the Group we want.
     * @return the Group object at that position.
     */
    @Override
    public Group getItem(int position) {
        return groupList.get(position);
    }

    /**
     * Get the id of the Group at position.
     * @param position the position of the Group we want to access.
     * @return the ID of the group at that position.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        Log.d(TAG, "Entered GroupListAdapter class.");
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.view_groups_in_address_book, null);

            // locate the views
            holder.groupName = (TextView) view.findViewById(R.id.groupName);
            holder.groupContactCount = (TextView) view.findViewById(R.id.contactCount);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Log.d(TAG, "Group(" + groupList.get(position).getGroupID() + ") contact count = " + groupList.get(position).getGroupContactList().size());
        // Set the results into TextViews
        holder.groupName.setText(groupList.get(position).getGroupName());
        String count = "(" + groupList.get(position).getContactCount() + ")";
        holder.groupContactCount.setText(count);

        // Listen for ListView Item Click
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // Send single item click data to SingleItemView Class
                Intent intent = new Intent(mContext, ViewGroupActivity.class);
                // Pass all data rank
                intent.putExtra("currentGroupID", (groupList.get(position).getGroupID()));
                // Start SingleItemView Class
                mContext.startActivity(intent);
                Log.d(TAG, "Clicked on Group \"" + groupList.get(position).getGroupName() + "\"");
            }
        });
        return view;
    }

    /**
     * Help alphabetize the Groups.
     * @param charText used to compare the name of one Group against another.
     */
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        groupList.clear();
        if (charText.length() == 0) {
            groupList.addAll(arraylist);
        } else {
            for (Group group : arraylist) {
                if (group.getGroupName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    groupList.add(group);
                }
            }
        }
        notifyDataSetChanged();
    }
}