package com.example.contactlist.contactlist;

import java.util.List;

/**
 * Created by Shelley on 11/12/2015.
 * Source code provided by Turki.
 * This class stores all the information about a Group.
 */
public class Group {

    private String groupName;
    private List<Contact> groupList;
    int groupID;


    public Group(int groupID, String groupName, List<Contact> groupList){
        this.groupID = groupID;
        this.groupName = groupName;
        this.groupList = groupList;
    }

    /**
     * This method returns the Group ID.
     * @return the ID of the Group.
     */
    public int getGroupID() {
        return groupID;
    }

    /**
     * Get the name of the Group.
     * @return the name of the group.
     */
    public String  getGroupName() {
        return groupName;
    }

    /**
     * Change the group name.
     * @param newName The new name of the Group.
     */
    public void setGroupName(String newName) {
        groupName = newName;
    }

    /**
     * Get an ArrayList of all the contacts in a Group.
     * @return the ArrayList of all Contacts in this Group.
     */
    public List<Contact> getGroupContactList() {
        return groupList;
    }

    /**
     * Update the list of Contacts that are in the Group.
     * @param groupList the ArrayList of Contacts in the Group.
     */
    public void setGroupList(List<Contact> groupList) {
        this.groupList = groupList;
    }

    /**
     * Get the total number of contacts within a group.
     * @return The number of contacts in that group.
     */
    public int getContactCount() {
        return groupList.size();
    }

    /**
     * Check to see if a Contact is in this Group.
     * @param contact The Contact we want to check for.
     * @return true if Contact exists, false otherwise.
     */
    public boolean contains(Contact contact) {
        for (int i = 0; i < groupList.size(); i++) {
            if (groupList.get(i).getId() == contact.getId()) {
                return true;
            }
        }
        return false;
    }
}