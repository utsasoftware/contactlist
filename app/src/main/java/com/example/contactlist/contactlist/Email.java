package com.example.contactlist.contactlist;

/**
 * Created by Iram Lee on 10/19/15.
 * Modified by Shelley Klinzing on 11/05/15.
 * This class represents the email information of a contact.
 */
public class Email {
    private int emailID;
    private String emailLabel, emailAddress;

    /*
    * Constructor:
    * ------------
    * Creates a new Email object
    * */
    public Email(int emailID, String emailLabel, String emailAddress){
        this.emailID = emailID;
        this.emailLabel = emailLabel;
        this.emailAddress = emailAddress;
    }

    /**
     * Set the Email id to be the same as the Contact id.
     * @param emailID The id of the contact.
     */
    public void setEmailID(int emailID) {
        this.emailID = emailID;
    }

    /*
    * Method: setEmail()
    * ------------------
    * Sets an email label and email address to the object.
    * Both values are of String type
    * */
    public boolean setEmail(String label, String email){
        emailLabel = label;
        emailAddress = email;
        return true;
    }

    /*
    * Method: getEmailID()
    * ------------------
    * Returns the email id (the contact id).
    * */
    public int getEmailID(){
        return emailID;
    }

    /*
    * Method: getEmailLabel()
    * ------------------
    * Returns the email address label.
    * */
    public String getEmailLabel(){
        return emailLabel;
    }

    /*
    * Method: getEmailAddress()
    * ------------------
    * Returns the email address.
    * */
    public String getEmailAddress(){
        return emailAddress;
    }

}