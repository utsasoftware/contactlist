package com.example.contactlist.contactlist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * This is the main activity. This is the first class that will run when we run the app.
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "__MainActivity: ";

    ListView contactListView; // the actual list of contacts
    ListView groupListView; // the list of all groups
    DatabaseHandler dbHandler; // handler for SQLite database
    ContactListAdapter adapter; // used for rendering the list of contacts
    GroupListAdapter groupAdapter; // used for rendering the list of groups
    List<Contact> contactList; // the list of all contacts in the address book
    List<Group> groupList; // the list of all the groups in the address book
    TextView hasContacts; // displays "You have no contacts." if there are no contacts.
    TextView hasGroups; // displays "You have no groups." if there are no groups.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "Entered onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        contactListView = (ListView) findViewById(R.id.contactListView);
        groupListView = (ListView) findViewById(R.id.groupListView);

        dbHandler = new DatabaseHandler(getApplicationContext());

        contactList = dbHandler.getAllContacts(); // grab all contacts
        groupList = dbHandler.getAllGroups(); // grab all groups
        Collections.sort(contactList, new ContactNameComparator()); // sort contacts alphabetically
        Collections.sort(groupList, new GroupNameComparator()); // sort groups alphabetically

        // handle the contact tabs and group tabs
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        // create contact tab
        TabHost.TabSpec tabSpec = tabHost.newTabSpec("contacts");
        tabSpec.setContent(R.id.contacts);
        tabSpec.setIndicator("Contacts");
        tabHost.addTab(tabSpec);

        // create group tab
        tabSpec = tabHost.newTabSpec("groups");
        tabSpec.setContent(R.id.groups);
        tabSpec.setIndicator("Groups");
        tabHost.addTab(tabSpec);

        hasContacts = (TextView) findViewById(R.id.hasContacts);
        if (contactList.size() == 0) {
            hasContacts.setVisibility(View.VISIBLE);
        } else {
            hasContacts.setVisibility(View.GONE);
        }

        hasGroups = (TextView) findViewById(R.id.hasGroups);
        if (groupList.size() == 0) {
            hasGroups.setVisibility(View.VISIBLE);
        } else {
            hasGroups.setVisibility(View.GONE);
        }

        // render the contact list
        adapter = new ContactListAdapter(this, contactList);
        contactListView.setAdapter(adapter);

        // render the group list
        groupAdapter = new GroupListAdapter(this, groupList);
        groupListView.setAdapter(groupAdapter);

        // search the contacts
        final EditText searchContacts = (EditText) findViewById(R.id.searchContacts);
        searchContacts.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = searchContacts.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }
        });

        // search the groups
        final EditText searchGroups = (EditText) findViewById(R.id.searchGroups);
        searchGroups.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                String text = searchGroups.getText().toString().toLowerCase(Locale.getDefault());
                groupAdapter.filter(text);
            }
        });

        /**
         * Click on contacts on the list to view the contact detail.
         */
        contactListView.setClickable(true);
        contactListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // grab the selected contact's information
                Contact currentContact = (Contact) parent.getAdapter().getItem(position);

                Log.v(TAG, "Clicked on contact \"" + currentContact.getName() + "\" with id: " + currentContact.getId());

                // start the intent to view the contact detail (ViewContactActivity)
                Intent viewContactActivityIntent = new Intent(view.getContext(), ViewContactActivity.class);
                viewContactActivityIntent.putExtra("currentContactID", currentContact.getId());
                startActivity(viewContactActivityIntent);
                finish();
            }
        });
    } // end of onCreate()

    /**
     * Set the options for the Settings menu. This handles action bar item clicks. The action bar
     * will automatically handle clicks on the Home/Up button, so long as you specify a parent
     * activity in AndroidManifest.xml.
     * @param item the item in the menu selected.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // switch statement to determine what action goes with what menu item
        switch (item.getItemId()) {
            case R.id.add_contact: // initiate add contact activity
                Log.v(TAG, "Starting AddContactActivity.");
                Intent addContactActivity = new Intent(getApplicationContext(),
                    AddContactActivity.class);
                startActivity(addContactActivity);
                finish();
                return true;
            case R.id.add_group: // create a new group
                Log.v(TAG, "Starting CreateGroupActivity.");
                Intent createGroupActivity = new Intent(getApplicationContext(),
                        CreateGroupActivity.class);
                startActivity(createGroupActivity);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * this method helps populate our settings menu, where there will be add contact, search, etc.
     * note this is for the contact LIST menu, not individual contacts
     * @param menu the menu object we want to populate.
     * @return true if sucess, false otherwise.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * When we return to MainActivity from another activity...
     */
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();

        // update contact list
        contactList.clear();
        contactList.addAll(dbHandler.getAllContacts());
        Collections.sort(contactList, new ContactNameComparator());
        adapter.notifyDataSetChanged();

        if (contactList.size() == 0) {
            hasContacts.setVisibility(View.VISIBLE);
        } else {
            hasContacts.setVisibility(View.GONE);
        }

        // update group list
        groupList.clear();
        groupList.addAll(dbHandler.getAllGroups());
        Collections.sort(groupList, new GroupNameComparator());
        groupAdapter.notifyDataSetChanged();

        if (groupList.size() == 0) {
            hasGroups.setVisibility(View.VISIBLE);
        } else {
            hasGroups.setVisibility(View.GONE);
        }
    }

    /**
     * Want to return to the main contact list on back pressed.
     */
    public void onBackPressed() {
        this.finish();
        Log.d(TAG, "Killed MainActivity on back pressed.");
    }
}