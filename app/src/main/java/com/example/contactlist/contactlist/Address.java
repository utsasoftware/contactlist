package com.example.contactlist.contactlist;

/**
 * Created by iramlee on 10/21/15.
 * Representation of an Address object. This will contain
 * the address information for one contact in the contact
 * list
 */
public class Address {

    private String _addressLabel, _addressLine1, _addressLine2, _zipCode;
    private String _state, _city, _country;

/*
* Constructor:
* ------------
* Creates a new Address object
* */
    public Address(String addressLabel, String addressLine1, String addressLine2, String zipCode,
                   String state, String city, String country){
        _addressLabel = addressLabel;
        _addressLine1 = addressLine1;
        _addressLine2 = addressLine2;
        _zipCode = zipCode;
        _state = state;
        _city = city;
        _country = country;
    }

/*
* Method: setAddress()
* --------------------
* Sets the address information
* */
    public boolean setAddress(String label, String addrL1, String addrL2, String zip,
                              String state, String city, String country){
        _addressLabel = label;
        _addressLine1 = addrL1;
        _addressLine2 = addrL2;
        _zipCode = zip;
        _state = state;
        _city = city;
        _country = country;
        return true;
    }

/*
* Method: getAddress()
* --------------------
* Returns the Address object
* */
    public Address getAddress(){
        return this;
    }
}
