package com.example.contactlist.contactlist;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Shelley Klinzing on 11/06/15.
 * This is the activity to edit a contact's information.
 */
public class EditContactActivity extends AppCompatActivity {

    private static final String TAG = "__EditContactActivity: "; // for logging

    private static final int ADD_IMAGE = 1;
    private static final int BIRTHDAY_DATE_DIALOG_ID = 3; // used for birthday date picker
    private static final int PHONE = 4;
    private static final int EMAIL = 5;
    private static final int BLACKLIST_FALSE = 0;
    private static final int BLACKLIST_TRUE = 1;

    Contact contact;
    private EditText nameTxt;
    private EditText phoneTxt1, phoneTxt2, phoneTxt3, phoneTxt4;
    private EditText emailTxt1, emailTxt2, emailTxt3, emailTxt4;
    private EditText addressTxt;
    private TextView birthdayDateView; // for the contact's birthday
    private ImageView contactImageImgView; // this displays the contact image
    private String encodedImageString = ""; // encoded string we will decode to get the contact image
    Spinner phoneLabelDropDownMenu1, phoneLabelDropDownMenu2, phoneLabelDropDownMenu3, phoneLabelDropDownMenu4;
    Spinner emailLabelDropDownMenu1, emailLabelDropDownMenu2, emailLabelDropDownMenu3, emailLabelDropDownMenu4;
    LinearLayout emailLayout1, emailLayout2, emailLayout3, emailLayout4;
    LinearLayout phoneNumLayout1, phoneNumLayout2, phoneNumLayout3, phoneNumLayout4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Entered onCreate()\n");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact);

        nameTxt = (EditText) findViewById(R.id.txtName);
        phoneTxt1 = (EditText) findViewById(R.id.txtPhone1);
        phoneTxt2 = (EditText) findViewById(R.id.txtPhone2);
        phoneTxt3 = (EditText) findViewById(R.id.txtPhone3);
        phoneTxt4 = (EditText) findViewById(R.id.txtPhone4);
        emailTxt1 = (EditText) findViewById(R.id.txtEmail1);
        emailTxt2 = (EditText) findViewById(R.id.txtEmail2);
        emailTxt3 = (EditText) findViewById(R.id.txtEmail3);
        emailTxt4 = (EditText) findViewById(R.id.txtEmail4);
        addressTxt = (EditText) findViewById(R.id.txtAddress);
        birthdayDateView = (TextView) findViewById(R.id.bithdayDisplayDateTextView);
        contactImageImgView = (ImageView) findViewById(R.id.imgViewContactImage);

        initializeSpinner(); // set up the drop down menus
        initializeLinearLayout(); // set up the LinearLayouts for viewing phones/emails

        final DatabaseHandler dbHandler = new DatabaseHandler(getApplicationContext());
        int contactID = getIntent().getExtras().getInt("currentContactID");
        Log.d(TAG, "The contact id extra was: " + contactID);
        contact = dbHandler.getContact(contactID); // get the contact object

        // let's format the phone number to look pretty, e.g., (210) 432-2231
        phoneTxt1.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        phoneTxt2.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        phoneTxt3.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        phoneTxt4.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        // fill in the layout with the current contact information
        if (contact != null) {
            Log.d(TAG, "Contact name is \"" + contact.getName() + "\"");
            Log.d(TAG, "ID (object): " + contact.getId() + "; ID (DB): " + contactID);

            // print the contact name
            nameTxt.setText(contact.getName());

            displayPhoneNumbers(contact); // display the current phone numbers
            displayEmailAddresses(contact); // display the current email addresses

            if(!contact.getAddress().equals("")) { // if there is an actual address
                addressTxt.setText(contact.getAddress());
            }

            if(!contact.getBirthday().equals("")) { // if there is actually a birthday
                birthdayDateView.setText(contact.getBirthday());
            }

            // decode and view contact image
            String encodedImg = contact.getImage(); // grab image
            byte[] bytarray = Base64.decode(encodedImg, Base64.DEFAULT);
            Bitmap contactImg = BitmapFactory.decodeByteArray(bytarray, 0, bytarray.length);
            contactImageImgView.setImageBitmap(contactImg); // display image

        } else {
            Log.d("ViewContactActivity","Contact is null!");
        }

        // this is the listener for allowing the user to select an image from their phone gallery
        contactImageImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Clicked on contact image to select image from phone gallery.");
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.putExtra("crop", "true");
                intent.putExtra("outputX", 100);
                intent.putExtra("outputY", 100);
                intent.putExtra("scale", true);
                intent.putExtra("return-data", true);
                startActivityForResult(intent, ADD_IMAGE);
            }

        });

        // create the listener for the add phone number button
        final ImageButton buttonAddPhoneNumber = (ImageButton) findViewById(R.id.addPhoneNumber);
        buttonAddPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Add a new phone number.");
                if (phoneNumLayout1.getVisibility() == View.GONE) {
                    phoneNumLayout1.setVisibility(view.VISIBLE);
                    Log.d(TAG, "Added phone number 1.");
                } else if (phoneNumLayout2.getVisibility() == View.GONE) {
                    phoneNumLayout2.setVisibility(view.VISIBLE);
                    Log.d(TAG, "Added phone number 2.");
                } else if (phoneNumLayout3.getVisibility() == View.GONE) {
                    phoneNumLayout3.setVisibility(view.VISIBLE);
                    Log.d(TAG, "Added phone number 3.");
                } else if (phoneNumLayout4.getVisibility() == View.GONE) {
                    phoneNumLayout4.setVisibility(view.VISIBLE);
                    Log.d(TAG, "Added phone number 4.");
                } else {
                    Log.d(TAG, "Maximum phone numbers reached.");
                    Toast.makeText(EditContactActivity.this, "Maximum Phone Numbers Reached",
                            Toast.LENGTH_SHORT).show();
                }
            }
        }); // end buttonAddPhoneNumber listener

        // create the listener for the add email button
        final ImageButton buttonAddEmail = (ImageButton) findViewById(R.id.addEmailButton);
        buttonAddEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Add a new email address.");
                if (emailLayout1.getVisibility() == View.GONE) {
                    emailLayout1.setVisibility(view.VISIBLE);
                    Log.d(TAG, "Added email 1.");
                } else if (emailLayout2.getVisibility() == View.GONE) {
                    emailLayout2.setVisibility(view.VISIBLE);
                    Log.d(TAG, "Added email 2.");
                } else if (emailLayout3.getVisibility() == View.GONE) {
                    emailLayout3.setVisibility(view.VISIBLE);
                    Log.d(TAG, "Added email 3.");
                } else if (emailLayout4.getVisibility() == View.GONE) {
                    emailLayout4.setVisibility(view.VISIBLE);
                    Log.d(TAG, "Added email 4.");
                } else {
                    Log.d(TAG, "Maximum Email Addresses reached.");
                    Toast.makeText(EditContactActivity.this, "Maximum Email Addresses Reached",
                            Toast.LENGTH_SHORT).show();
                }
            }
        }); // end buttonAddEmail listener

        /*
         * Listener to save the edited contact. Will update the database and then return to contact
         * detail view.
         */
        final Button buttonSaveEditedContact = (Button) findViewById(R.id.buttonSave);
        buttonSaveEditedContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Clicked Saved Edit Contact Button");

                contact.setName(String.valueOf(nameTxt.getText())); // set new name
                contact.setAddress(String.valueOf(addressTxt.getText())); // set new address
                contact.setBirthday(String.valueOf(birthdayDateView.getText())); // set new birthday
                // if contact has a new image, update the encoded image string.
                if (!encodedImageString.equals("")) {
                    contact.setImage(encodedImageString); // set new image
                }

                // grab all the new phone numbers and email addresses
                // create phone array list
                List<Phone> phoneNumbers = new ArrayList<Phone>();
                // add to ArrayList
                if (!String.valueOf(phoneTxt1.getText()).equals("")) {
                    phoneNumbers.add(new Phone((0),
                            phoneLabelDropDownMenu1.getSelectedItem().toString(),
                            String.valueOf(phoneTxt1.getText())));
                }
                if (!String.valueOf(phoneTxt2.getText()).equals("")) {
                    phoneNumbers.add(new Phone((0),
                            phoneLabelDropDownMenu2.getSelectedItem().toString(),
                            String.valueOf(phoneTxt2.getText())));
                }
                if (!String.valueOf(phoneTxt3.getText()).equals("")) {
                    phoneNumbers.add(new Phone((0),
                            phoneLabelDropDownMenu3.getSelectedItem().toString(),
                            String.valueOf(phoneTxt3.getText())));
                }
                if (!String.valueOf(phoneTxt4.getText()).equals("")) {
                    phoneNumbers.add(new Phone((0),
                            phoneLabelDropDownMenu4.getSelectedItem().toString(),
                            String.valueOf(phoneTxt4.getText())));
                }
                // update the contact's phone numbers
                contact.updatePhoneNumbers(phoneNumbers);

                // create email array list
                List<Email> emailAddresses = new ArrayList<Email>();
                if (!String.valueOf(emailTxt1.getText()).equals("")) {
                    emailAddresses.add(new Email((0),
                            emailLabelDropDownMenu1.getSelectedItem().toString(),
                            String.valueOf(emailTxt1.getText())));
                }
                if (!String.valueOf(emailTxt2.getText()).equals("")) {
                    emailAddresses.add(new Email((0),
                            emailLabelDropDownMenu2.getSelectedItem().toString(),
                            String.valueOf(emailTxt2.getText())));
                }
                if (!String.valueOf(emailTxt3.getText()).equals("")) {
                    emailAddresses.add(new Email((0),
                            emailLabelDropDownMenu3.getSelectedItem().toString(),
                            String.valueOf(emailTxt3.getText())));
                }
                if (!String.valueOf(emailTxt4.getText()).equals("")) {
                    emailAddresses.add(new Email((0),
                            emailLabelDropDownMenu4.getSelectedItem().toString(),
                            String.valueOf(emailTxt4.getText())));
                }
                // update the contact's email addresses
                contact.updateEmailAddresses(emailAddresses);

                dbHandler.updateContact(contact); // update the contact in the database

                Log.d(TAG, "Contact id = " + contact.getId());
                // return to the contact detail
                Intent viewContactDetailActivity = new Intent(getApplicationContext(),
                        ViewContactActivity.class);
                viewContactDetailActivity.putExtra("currentContactID", contact.getId());
                startActivity(viewContactDetailActivity);
                finish();

                Toast.makeText(getApplicationContext(), String.valueOf(nameTxt.getText())
                        + " has been updated.", Toast.LENGTH_SHORT).show();
            }
        });

        /*
         * Listener to return back to Contact Detail if user clicks "Cancel".
         */
        final Button buttonCancelEditedContact = (Button) findViewById(R.id.buttonCancel);
        buttonCancelEditedContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Clicked Cancel Edit Contact Button");
                Intent viewContactDetail = new Intent(getApplicationContext(),
                        ViewContactActivity.class);
                viewContactDetail.putExtra("currentContactID", contact.getId());
                startActivity(viewContactDetail);
                finish();
            }
        });

        // this will make sure that you can't add a blank contact
        nameTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                buttonSaveEditedContact.setEnabled(String.valueOf(nameTxt.getText()).trim().length() > 0);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        }); // end text change listener

        // determine if the contact is on the blacklist or not
        Switch switchBlackList = (Switch) findViewById(R.id.switchBlackList);
        if (contact.isBlackListed() == BLACKLIST_FALSE) {
            switchBlackList.setChecked(false);
        } else {
            switchBlackList.setChecked(true);
        }
        switchBlackList.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){ // true
                    contact.setBlacklist(BLACKLIST_TRUE);
                }else{
                    contact.setBlacklist(BLACKLIST_FALSE);
                }
            }
        });
    } // end onCreate()

    /**
     * This method helps us decide what to do on return from another activity.
     * @param reqCode The type of request we are dealing with.
     * @param resCode The result code. Is is okay, or bad?
     * @param data Data that we may or may not use passed on from another activity.
     */
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        Log.d(TAG, "Entered onActivityResult()");
        Log.d(TAG, "reqCode = " + reqCode + "; resCode = " + resCode + "; RESULT_OK = " + RESULT_OK);
        // grab the chosen contact image URI on return of the add contact activity
        if (resCode == RESULT_OK && reqCode == ADD_IMAGE && data != null) {
            Log.d(TAG, "The user selected a contact image.");

            // grab the image from the return results
            Bitmap bmp = (Bitmap) data.getExtras().get("data");

            // convert it to an array and then encode it into a string
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if (bmp != null) {
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            }
            byte[] b = baos.toByteArray();
            encodedImageString = Base64.encodeToString(b, Base64.DEFAULT);

            // display a preview of the image
            contactImageImgView.setImageBitmap(bmp);
            Toast.makeText(getBaseContext(), "Added your chosen image", Toast.LENGTH_SHORT).show();
        }
    } // end onActivityResult()

    /**
     * This method helps us fill in all of the contact's current phone numbers.
     * @param contact The current contact.
     */
    private void displayPhoneNumbers(Contact contact) {
        // grab the spinner information
        List<Spinner> phoneDropDownMenuList = new ArrayList<Spinner>();
        phoneDropDownMenuList.add(phoneLabelDropDownMenu1);
        phoneDropDownMenuList.add(phoneLabelDropDownMenu2);
        phoneDropDownMenuList.add(phoneLabelDropDownMenu3);
        phoneDropDownMenuList.add(phoneLabelDropDownMenu4);

        // grab the phone number EditText views
        List<EditText> phoneNumberEditTextViews= new ArrayList<EditText>();
        phoneNumberEditTextViews.add(phoneTxt1);
        phoneNumberEditTextViews.add(phoneTxt2);
        phoneNumberEditTextViews.add(phoneTxt3);
        phoneNumberEditTextViews.add(phoneTxt4);

        // grab the layout to toggle visibility
        List<LinearLayout> phoneNumLayoutList = new ArrayList<>();
        phoneNumLayoutList.add(phoneNumLayout1);
        phoneNumLayoutList.add(phoneNumLayout2);
        phoneNumLayoutList.add(phoneNumLayout3);
        phoneNumLayoutList.add(phoneNumLayout4);

        // set the contact phone numbers
        List<Phone> phoneNumbers = contact.getPhoneNumbers(); // grab all contact phone numbers
        if (phoneNumbers.size() > 0) {
            for (int i = 0; i < phoneNumbers.size(); i++) {
                if (!phoneNumbers.get(i).getPhoneNumber().equals("")) {
                    phoneNumLayoutList.get(i).setVisibility(View.VISIBLE); // toggle visibility
                    switch(phoneNumbers.get(i).getPhoneLabel()) { // set the label
                        case "Mobile":
                            phoneDropDownMenuList.get(i).setSelection(0);
                            break;
                        case "Home":
                            phoneDropDownMenuList.get(i).setSelection(1);
                            break;
                        case "Work":
                            phoneDropDownMenuList.get(i).setSelection(2);
                            break;
                        case "Other":
                            phoneDropDownMenuList.get(i).setSelection(3);
                            break;
                    }
                    // set the phone number
                    phoneNumberEditTextViews.get(i).setText(phoneNumbers.get(i).getPhoneNumber());
                }
            }
        }
    } // end displayPhoneNumbers()

    /**
     * This method helps us fill in all of the contact's current email addresses.
     * @param contact The current contact.
     */
    private void displayEmailAddresses(Contact contact) {
        // grab the spinner information
        List<Spinner> emailDropDownMenuList = new ArrayList<Spinner>();
        emailDropDownMenuList.add(emailLabelDropDownMenu1);
        emailDropDownMenuList.add(emailLabelDropDownMenu2);
        emailDropDownMenuList.add(emailLabelDropDownMenu3);
        emailDropDownMenuList.add(emailLabelDropDownMenu4);

        // grab the phone number EditText views
        List<EditText> emailEditTextViews = new ArrayList<EditText>();
        emailEditTextViews.add(emailTxt1);
        emailEditTextViews.add(emailTxt2);
        emailEditTextViews.add(emailTxt3);
        emailEditTextViews.add(emailTxt4);

        // grab the layout to toggle visibility
        List<LinearLayout> emailLayoutList = new ArrayList<>();
        emailLayoutList.add(emailLayout1);
        emailLayoutList.add(emailLayout2);
        emailLayoutList.add(emailLayout3);
        emailLayoutList.add(emailLayout4);

        // set the contact phone numbers
        List<Email> emailAddresses = contact.getEmailAddresses(); // grab all contact email addresses
        if (emailAddresses.size() > 0) {
            for (int i = 0; i < emailAddresses.size(); i++) {
                if (!emailAddresses.get(i).getEmailAddress().equals("")) {
                    emailLayoutList.get(i).setVisibility(View.VISIBLE); // toggle visibility
                    switch(emailAddresses.get(i).getEmailLabel()) { // set the label
                        case "Home":
                            emailDropDownMenuList.get(i).setSelection(1);
                            break;
                        case "Work":
                            emailDropDownMenuList.get(i).setSelection(2);
                            break;
                        case "Other":
                            emailDropDownMenuList.get(i).setSelection(3);
                            break;
                    }
                    // set the phone number
                    emailEditTextViews.get(i).setText(emailAddresses.get(i).getEmailAddress());
                }
            }
        }
    } // end displayEmailAddresses()

    /**
     * Helper method to initialize all the drop down menus, since it involves so much code.
     */
    private void initializeSpinner() {
        // create the four drop down menus for the phone labels
        phoneLabelDropDownMenu1 = (Spinner) findViewById(R.id.phoneLabel1);
        phoneLabelDropDownMenu2 = (Spinner) findViewById(R.id.phoneLabel2);
        phoneLabelDropDownMenu3 = (Spinner) findViewById(R.id.phoneLabel3);
        phoneLabelDropDownMenu4 = (Spinner) findViewById(R.id.phoneLabel4);
        phoneLabelDropDownMenu1.setAdapter(createLabelAdapter(PHONE));
        phoneLabelDropDownMenu2.setAdapter(createLabelAdapter(PHONE));
        phoneLabelDropDownMenu3.setAdapter(createLabelAdapter(PHONE));
        phoneLabelDropDownMenu4.setAdapter(createLabelAdapter(PHONE));

        // create the four drop down menus for the email labels
        emailLabelDropDownMenu1 = (Spinner) findViewById(R.id.emailLabel1);
        emailLabelDropDownMenu2 = (Spinner) findViewById(R.id.emailLabel2);
        emailLabelDropDownMenu3 = (Spinner) findViewById(R.id.emailLabel3);
        emailLabelDropDownMenu4 = (Spinner) findViewById(R.id.emailLabel4);
        emailLabelDropDownMenu1.setAdapter(createLabelAdapter(EMAIL));
        emailLabelDropDownMenu2.setAdapter(createLabelAdapter(EMAIL));
        emailLabelDropDownMenu3.setAdapter(createLabelAdapter(EMAIL));
        emailLabelDropDownMenu4.setAdapter(createLabelAdapter(EMAIL));

        // create a listener for the phone label drop down menu
        phoneLabelDropDownMenu1.setOnItemSelectedListener(new MySpinnerOnItemSelectedListener());
        phoneLabelDropDownMenu2.setOnItemSelectedListener(new MySpinnerOnItemSelectedListener());
        phoneLabelDropDownMenu3.setOnItemSelectedListener(new MySpinnerOnItemSelectedListener());
        phoneLabelDropDownMenu4.setOnItemSelectedListener(new MySpinnerOnItemSelectedListener());

        // create a listener for the email label drop down menu
        emailLabelDropDownMenu1.setOnItemSelectedListener(new MySpinnerOnItemSelectedListener());
        emailLabelDropDownMenu2.setOnItemSelectedListener(new MySpinnerOnItemSelectedListener());
        emailLabelDropDownMenu3.setOnItemSelectedListener(new MySpinnerOnItemSelectedListener());
        emailLabelDropDownMenu4.setOnItemSelectedListener(new MySpinnerOnItemSelectedListener());
    }

    /**
     * Helper method to initialize all the linear layouts (used for viewing phone numbers and
     * email addresses).
     */
    private void initializeLinearLayout() {
        // phone number linear layouts
        phoneNumLayout1 = (LinearLayout) findViewById(R.id.phoneNumberLinearLayout_1);
        phoneNumLayout2 = (LinearLayout) findViewById(R.id.phoneNumberLinearLayout_2);
        phoneNumLayout3 = (LinearLayout) findViewById(R.id.phoneNumberLinearLayout_3);
        phoneNumLayout4 = (LinearLayout) findViewById(R.id.phoneNumberLinearLayout_4);

        // email address linear layouts
        emailLayout1 = (LinearLayout) findViewById(R.id.emailLinearLayout_1);
        emailLayout2 = (LinearLayout) findViewById(R.id.emailLinearLayout_2);
        emailLayout3 = (LinearLayout) findViewById(R.id.emailLinearLayout_3);
        emailLayout4 = (LinearLayout) findViewById(R.id.emailLinearLayout_4);
    }

    /**
     * This helps populate the drop down menu with the label choices. (phone, email)
     * @return An array adapter of labels.
     */
    public ArrayAdapter<CharSequence> createLabelAdapter(int type) {
        ArrayAdapter<CharSequence> adapter = null;
        switch(type) {
            case PHONE:
                adapter = ArrayAdapter.createFromResource(this,
                        R.array.phone_label_array, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                return adapter;
            case EMAIL:
                adapter = ArrayAdapter.createFromResource(this,
                        R.array.email_label_array, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                return adapter;
        }
        return adapter; // we shouldn't reach this ever
    }

    /**
     * The onClick method for the drop down menus for the labels
     */
    private class MySpinnerOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Spinner spinner = (Spinner) parent;
            switch (spinner.getId()) {
                case R.id.phoneLabel1:
                    Log.d(TAG, "Selected phone label (1): " + parent.getItemAtPosition(position).toString());
                    break;
                case R.id.phoneLabel2:
                    Log.d(TAG, "Selected phone label (2): " + parent.getItemAtPosition(position).toString());
                    break;
                case R.id.phoneLabel3:
                    Log.d(TAG, "Selected phone label (3): " + parent.getItemAtPosition(position).toString());
                    break;
                case R.id.phoneLabel4:
                    Log.d(TAG, "Selected phone label (4): " + parent.getItemAtPosition(position).toString());
                    break;
                case R.id.emailLabel1:
                    Log.d(TAG, "Selected email label (1): " + parent.getItemAtPosition(position).toString());
                    break;
                case R.id.emailLabel2:
                    Log.d(TAG, "Selected email label (2): " + parent.getItemAtPosition(position).toString());
                    break;
                case R.id.emailLabel3:
                    Log.d(TAG, "Selected email label (3): " + parent.getItemAtPosition(position).toString());
                    break;
                case R.id.emailLabel4:
                    Log.d(TAG, "Selected email label (4): " + parent.getItemAtPosition(position).toString());
                    break;
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing
        }
    }

    /**
     * Remove a field (phone number/email) on button click.
     * This is called from the layout file (using android:onClick)
     * @param view EditContactACtivity view.
     */
    public void removeField(View view) {
        Log.d(TAG, "Remove a field.");
        switch(view.getId()) {
            case R.id.delPhoneNumber1:
                phoneNumLayout1.setVisibility(view.GONE); // toggle visibility off
                phoneTxt1.setText(""); // clear the field
                phoneLabelDropDownMenu1.setSelection(0); // revert to first selection
                Log.d(TAG, "Removed phone number 1.");
                break;
            case R.id.delPhoneNumber2:
                phoneNumLayout2.setVisibility(view.GONE); // toggle visibility off
                phoneTxt2.setText(""); // clear the field
                phoneLabelDropDownMenu2.setSelection(0); // revert to first selection
                Log.d(TAG, "Removed phone number 2.");
                break;
            case R.id.delPhoneNumber3:
                phoneNumLayout3.setVisibility(view.GONE); // toggle visibility off
                phoneTxt3.setText(""); // clear the field
                phoneLabelDropDownMenu3.setSelection(0); // revert to first selection
                Log.d(TAG, "Removed phone number 3.");
                break;
            case R.id.delPhoneNumber4:
                phoneNumLayout4.setVisibility(view.GONE); // toggle visibility off
                phoneTxt4.setText(""); // clear the field
                phoneLabelDropDownMenu4.setSelection(0); // revert to first selection
                Log.d(TAG, "Removed phone number 4.");
                break;
            case R.id.delEmail1:
                emailLayout1.setVisibility(view.GONE); // toggle visibility off
                emailTxt1.setText(""); // clear the field
                emailLabelDropDownMenu1.setSelection(0); // revert to first selection
                Log.d(TAG, "Removed email number 1.");
                break;
            case R.id.delEmail2:
                emailLayout2.setVisibility(view.GONE); // toggle visibility off
                emailTxt2.setText(""); // clear the field
                emailLabelDropDownMenu2.setSelection(0); // revert to first selection
                Log.d(TAG, "Removed email number 2.");
                break;
            case R.id.delEmail3:
                emailLayout3.setVisibility(view.GONE); // toggle visibility off
                emailTxt3.setText(""); // clear the field
                emailLabelDropDownMenu3.setSelection(0); // revert to first selection
                Log.d(TAG, "Removed email number 3.");
                break;
            case R.id.delEmail4:
                emailLayout4.setVisibility(view.GONE); // toggle visibility off
                emailTxt4.setText(""); // clear the field
                emailLabelDropDownMenu4.setSelection(0); // revert to first selection
                Log.d(TAG, "Removed email number 4.");
                break;
        } // end switch
    } // end removeField()

    /**
     * Create the pop-up dialog to select birthday date.
     * @param id The ID of the dialog.
     * @return The created date dialog.
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        Log.d(TAG, "Entered onCreateDialog()");
        DatePickerDialog dateDlg = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        Time chosenDate = new Time();
                        chosenDate.set(dayOfMonth, monthOfYear, year);
                        long dtDob = chosenDate.toMillis(true);
                        CharSequence strBirthdayDate = DateFormat.format("MM-dd-yyyy", dtDob);

                        // display the date on the layout
                        birthdayDateView.setText(strBirthdayDate);
                    }}, 2015,0, 1);

        dateDlg.setMessage("Select Contact Birthday");
        return dateDlg;
    }

    /**
     * Prepare the date selector dialog for use.
     * I use this for the birthday date selector.
     * @param id The ID of the dialog.
     * @param dialog The date dialog we want to prepare for use.
     */
    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        Log.d(TAG, "Entered onPrepareDialog()");
        super.onPrepareDialog(id, dialog);
        DatePickerDialog dateDlg = (DatePickerDialog) dialog;
        int iDay,iMonth,iYear;

        // Always init the date picker to today's date
        Calendar cal = Calendar.getInstance();
        iDay = cal.get(Calendar.DAY_OF_MONTH);
        iMonth = cal.get(Calendar.MONTH);
        iYear = cal.get(Calendar.YEAR);
        dateDlg.updateDate(iYear, iMonth, iDay);
    }

    /**
     * When the add date button is clicked, display the date picker dialog.
     * @param v View v.
     */
    public void onDateDialogButtonClick(View v) {
        Log.d(TAG, "onDateDialogButtonClick()");
        showDialog(BIRTHDAY_DATE_DIALOG_ID);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
