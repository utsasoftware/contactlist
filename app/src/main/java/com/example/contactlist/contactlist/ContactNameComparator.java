package com.example.contactlist.contactlist;

import android.util.Log;

import java.util.Comparator;

/**
 * Created by Shelley on 11/10/2015.
 * Helps sort the contacts alphabetically.
 */
public class ContactNameComparator implements Comparator<Contact> {

    private static final String TAG = "__ContactNameCompare: ";

    @Override
    public int compare(Contact a, Contact b) {
        Log.d(TAG, "Entered ContactNameComparator class.");
        String nameA = a.getName();
        String nameB = b.getName();
        return nameA.replaceAll("\\s+", "").compareToIgnoreCase(nameB.replaceAll("\\s+", ""));
    }
}
