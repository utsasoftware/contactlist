package com.example.contactlist.contactlist;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by iramlee on 11/21/15.
 * This class tests all the functions in Address.java.
 */
public class TestAddress extends TestCase{
    String addressLabel, addressLine1, addressLine2, zipCode,
            state, city, country;
    Address address, testAddress, testAddress2;

    @Before
    public void setUp() throws Exception{
        addressLabel = "Home";
        addressLine1 = "600 Evergreen St.";
        addressLine2 = "none";
        zipCode = "78254";
        state = "IL";
        city = "Springfield";
        country = "USA";

        address = new Address(addressLabel, addressLine1, addressLine2, zipCode, state, city, country);
    }

    @After
    public void tearDown() throws Exception{

    }

    @Test
    public void testGetAddress() throws Exception{
        testAddress = address;
        assertEquals("Contact's address", testAddress.toString(), address.getAddress().toString());
    }
    @Test
    public void testSetAddress() throws Exception{
        String newLabel = "Work";
        String newAddrLine1 =  "Main st.";
        String newAddrLine2 = "";
        String newZip = "78352";
        String newState = "TX";
        String newCity = "San Antonio";
        String newCountry = "USA";
        assertTrue("New address set", address.setAddress(newLabel, newAddrLine1, newAddrLine2
                                                            ,newZip, newState, newCity, newCountry));
    }
}
