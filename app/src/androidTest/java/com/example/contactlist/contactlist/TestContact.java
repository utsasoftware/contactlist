package com.example.contactlist.contactlist;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Shelley on 11/20/2015.
 * This class tests all the functions in Contact.java
 */
public class TestContact extends TestCase {

    int id = 13;
    String name = "Sara";
    String addressC1 = "100 Red River, San Antonio, TX 78213";
    String birthdayC1 = "10-13-1992";
    List<Phone> phoneNumbers1;
    Phone phoneC1, phoneC2, phoneC3, phoneC4;
    List<Email> emailAddresses1;
    Email emailC1, emailC2, emailC3, emailC4;
    String encodedImageString;
    Contact contact;
    int blackList = 0; // 0 (false) by default

    @Before
    public void setUp() throws Exception {
        phoneNumbers1 = new ArrayList<>();
        phoneC1 = new Phone(0, "Mobile", "(210) 221-2121");
        phoneC2 = new Phone(0, "Home", "(210) 333-2121");
        phoneC3 = new Phone(0, "Other", "(210) 444-2121");
        phoneC4 = new Phone(0, "Work", "(210) 555-2121");
        phoneNumbers1.add(phoneC1);
        phoneNumbers1.add(phoneC2);
        phoneNumbers1.add(phoneC3);
        phoneNumbers1.add(phoneC4);
        emailAddresses1 = new ArrayList<>();
        emailC1 = new Email(0, "Work", "mail1@gmail.com");
        emailC2 = new Email(0, "Home", "mail2@gmail.com");
        emailC3 = new Email(0, "Other", "mail3@gmail.com");
        emailC4 = new Email(0, "Work", "mail4@gmail.com");
        emailAddresses1.add(emailC1);
        emailAddresses1.add(emailC2);
        emailAddresses1.add(emailC3);
        emailAddresses1.add(emailC4);
        encodedImageString = "random encoded string";

        contact = new Contact(id, name, phoneNumbers1, emailAddresses1, addressC1, birthdayC1
                                ,encodedImageString, 0);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetContactId() throws Exception {
        assertEquals("contact ID", id, contact.getId());
    }

    @Test
    public void testGetContactName() throws Exception {
        assertEquals("get contact name", name, contact.getName());
    }

    @Test
    public void testSetContactName() throws Exception {
        contact.setName("Mary");
        assertEquals("set contact name", "Mary", contact.getName());
    }

    @Test
    public void testGetEmailAddresses() throws Exception {
        for (int i = 0; i < emailAddresses1.size(); i++) {
            assertEquals("get email addresses", emailAddresses1.get(i), contact.getEmailAddresses().get(i));
        }
    }

    @Test
    public void testUpdateEmailAddresses() throws Exception {
        List<Email> testEmailAddresses = new ArrayList<>();
        Email testEmail1 = new Email(0, "Work", "testmail1@gmail.com");
        Email testEmail2 = new Email(0, "Home", "testmail2@gmail.com");
        Email testEmail3 = new Email(0, "Other", "testmail3@gmail.com");
        Email testEmail4 = new Email(0, "Work", "testmail4@gmail.com");
        testEmailAddresses.add(testEmail1);
        testEmailAddresses.add(testEmail2);
        testEmailAddresses.add(testEmail3);
        testEmailAddresses.add(testEmail4);
        contact.updateEmailAddresses(testEmailAddresses);
        for (int i = 0; i < testEmailAddresses.size(); i++) {
            assertEquals("update email addresses", testEmailAddresses.get(i), contact.getEmailAddresses().get(i));
        }
    }

    @Test
     public void testGetAddress() throws Exception {
        assertEquals("get addresses", addressC1, contact.getAddress());
    }

    @Test
    public void testSetAddress() throws Exception {
        String testAddress = "9400 Fred Rd, San Antonio, TX 78215";
        contact.setAddress(testAddress);
        assertEquals("get addresses", testAddress, contact.getAddress());
    }

    @Test
    public void testGetPhoneNumbers() throws Exception {
        for (int i = 0; i < phoneNumbers1.size(); i++) {
            assertEquals("get phone numbers", phoneNumbers1.get(i), contact.getPhoneNumbers().get(i));
        }
    }

    @Test
    public void testUpdatePhoneNumbers() throws Exception {
        List<Phone> testPhoneNumbers = new ArrayList<>();
        Phone testPhone1 = new Phone(0, "Home", "(210) 444-2121");
        Phone testPhone2 = new Phone(0, "Mobile", "(210) 555-2121");
        Phone testPhone3 = new Phone(0, "Work", "(210) 666-2121");
        Phone testPhone4 = new Phone(0, "Other", "(210) 777-2121");
        testPhoneNumbers.add(testPhone1);
        testPhoneNumbers.add(testPhone2);
        testPhoneNumbers.add(testPhone3);
        testPhoneNumbers.add(testPhone4);
        contact.updatePhoneNumbers(testPhoneNumbers);
        for (int i = 0; i < testPhoneNumbers.size(); i++) {
            assertEquals("update phone numbers", testPhoneNumbers.get(i), contact.getPhoneNumbers().get(i));
        }
    }

    @Test
    public void testGetBirthday() throws Exception {
        assertEquals("get birthday", birthdayC1, contact.getBirthday());
    }

    @Test
    public void testSetBirthday() throws Exception {
        String testBirthday = "11-27-1995";
        contact.setBirthday(testBirthday);
        assertEquals("set birthday", testBirthday, contact.getBirthday());
    }

    @Test
    public void testGetImage() throws Exception {
        assertEquals("get image", encodedImageString, contact.getImage());
    }

    @Test
    public void testSetImage() throws Exception {
        String testEncodedImageString = "another randome encoded image string";
        contact.setImage(testEncodedImageString);
        assertEquals("set image", testEncodedImageString, contact.getImage());
    }

    @Test
    public void testIsBlacklisted() throws Exception {
        assertEquals("is blacklisted?", blackList, contact.isBlackListed());
    }

    @Test
    public void testSetBlacklisted() throws Exception {
        contact.setBlacklist(1); // set to true
        assertEquals("is blacklisted?", 1, contact.isBlackListed());
    }
}