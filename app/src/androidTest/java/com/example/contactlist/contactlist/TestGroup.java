package com.example.contactlist.contactlist;

import junit.framework.TestCase;
import java.util.List;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Avid on 11/21/2015.
 * Updated by Iram on 11/23/2015.
 * This class tests all the functions in Group.java.
 */
public class TestGroup extends TestCase{
    int groupID = 1;
    String groupName = "groupUnderTest";
    List<Contact> groupList;
    Group testGroup;

    Contact contact1, contact2, contact3, fakeContact;
    Phone phoneC1, phoneC2, phoneC3;
    Email emailC1, emailC2, emailC3;
    List<Phone> phoneListC1, phoneListC2, phoneListC3;
    List<Email> emailListC1, emailListC2,emailListC3;

    @Before
    public void setUp() throws Exception{
        // Create all contacts and lists for the Group //
        phoneListC1 = new ArrayList<>();
        phoneListC2 = new ArrayList<>();
        phoneListC3 = new ArrayList<>();
        emailListC1 = new ArrayList<>();
        emailListC2 = new ArrayList<>();
        emailListC3 = new ArrayList<>();
        groupList = new ArrayList<>();
        phoneC1 = new Phone(1, "Work", "(210) 123-4567");
        phoneC2 = new Phone(2, "Home", "(210) 555-5555");
        phoneC3 = new Phone(3, "Work", "(123) 444-5555");
        emailC1 = new Email(1,"Work", "mail1@gmail.com");
        emailC2 = new Email(2, "Home", "mail2@gmail.com");
        emailC3 = new Email(3, "Work", "mail3@gmail.com");
        phoneListC1.add(phoneC1);
        phoneListC2.add(phoneC2);
        phoneListC3.add(phoneC3);
        emailListC1.add(emailC1);
        emailListC2.add(emailC2);
        emailListC3.add(emailC3);
        contact1 = new Contact(1, "John Smith", phoneListC1, emailListC1, "One UTSA Blvd", "2/23/1986", "image1", 0);
        contact2 = new Contact(2, "Jane Smith", phoneListC2, emailListC2, "876 Main St.", "5/12/1979", "image2", 0);
        contact3 = new Contact(3, "Homer S.", phoneListC3, emailListC3, "Evergreen Ave.", "8/19/1968", "image3", 0);
        fakeContact = new Contact(4, "No name", phoneListC3, emailListC3, "No street", "4/5/1879", "no image", 0);
        groupList.add(contact1);
        groupList.add(contact2);
        groupList.add(contact3);

        testGroup = new Group(groupID, groupName, groupList);
    }

    @After
    public void tearDown() throws Exception{

    }

    @Test
    public void testGetGroupId() throws Exception{
        assertEquals("Get group ID", groupID, testGroup.getGroupID());
    }

    @Test
    public void testGetGroupName() throws Exception{
        assertEquals("Get group name", groupName, testGroup.getGroupName());
    }

    @Test
    public void testGetGroupContactList() throws Exception{
        for(int i = 0; i < groupList.size(); i++){
            assertEquals("Get group list", groupList.get(i), testGroup.getGroupContactList().get(i));
        }
    }

    @Test
    public void testGetContactCount() throws Exception{
        assertEquals("Contacts count", groupList.size(), testGroup.getContactCount());
    }

    @Test
    public void testContains() throws Exception{
        assertTrue("Contains contact1", testGroup.contains(contact1));
        assertTrue("Contains contact2", testGroup.contains(contact2));
        assertTrue("Contains contact3", testGroup.contains(contact3));
        assertFalse("Does not contains fakeContact", testGroup.contains(fakeContact));
    }

    @Test
    public void testSetGroupName() throws Exception{
        String newGroupName = "Group 2";
        testGroup.setGroupName(newGroupName);
        assertEquals("Set new group name", newGroupName, testGroup.getGroupName());
    }

    @Test
    public void testSetGroupList() throws Exception{
        groupList.add(fakeContact);
        testGroup.setGroupList(groupList);
        for(int i = 0; i < groupList.size(); i++){
            assertEquals("Get group list", groupList.get(i), testGroup.getGroupContactList().get(i));
        }
    }
}
