package com.example.contactlist.contactlist;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Avid on 11/21/2015.
 * Updated by Iram on 11/23/2015.
 * This class tests all the functions in Email.java.
 */
public class TestEmail extends TestCase {
    int id;
    String emailLabel, emailAddress;
    Email testEmail;

    @Before
    public void setUp() throws Exception{
        id = 10;
        emailLabel = "Home";
        emailAddress = "email1@gmail.com";

        testEmail = new Email(id, emailLabel, emailAddress);
    }

    @After
    public void tearDown() throws Exception{

    }

    @Test
    public void testGetEmailID() throws Exception{
        assertEquals("Get email ID", id, testEmail.getEmailID());
    }

    @Test
    public void testGetEmailLabel() throws Exception{
        assertEquals("Get email label", emailLabel, testEmail.getEmailLabel());
    }

    @Test
    public void testGetEmailAddress() throws Exception{
        assertEquals("Get email address", emailAddress, testEmail.getEmailAddress());
    }

    @Test
    public void testSetEmailId() throws Exception{
        testEmail.setEmailID(3);
        assertEquals("Email ID set", 3, testEmail.getEmailID());
    }

    @Test
    public void testSetEmail() throws Exception{
        assertEquals("Email set", true, testEmail.setEmail("Work", "emailset@gmail.com"));
    }
}
