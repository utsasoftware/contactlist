package com.example.contactlist.contactlist;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.EmptyStackException;

import static org.junit.Assert.*;

/**
 * Created by iramlee on 11/20/15.
 * This class tests all the functions in Phone.java.
 */
public class TestPhone extends TestCase {
    int phoneID = 14;
    String phoneLabel;
    String phoneNumber;
    Phone phone;

    @Before
    public void setUp() throws Exception{
        phoneLabel = "Work";
        phoneNumber = "210-123-4567";
        phone = new Phone(phoneID, phoneLabel, phoneNumber);
    }

    @After
    public void tearDown() throws Exception{

    }
    @Test
    public void testGetPhoneId() throws Exception{
        assertEquals("Phone ID", phoneID, phone.getPhoneID());
    }
    @Test
    public void testSetPhoneID() throws Exception{
        phone.setPhoneID(10);
        assertEquals("Set new phone ID", 10, phone.getPhoneID());
    }
    @Test
    public void testGetPhoneNumber() throws Exception{
        assertEquals("Phone number", phoneNumber, phone.getPhoneNumber());
    }
    @Test
    public void testGetPhoneLabel() throws Exception{
        assertEquals("Phone label", phoneLabel, phone.getPhoneLabel());
    }

}
